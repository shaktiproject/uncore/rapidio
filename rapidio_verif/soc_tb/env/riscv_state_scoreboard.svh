//-------------------------------------------------------------------------------------------------- 
// Copyright (c) 2018, IIT Madras All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are permitted
// provided that the following conditions are met:
// 
// - Redistributions of source code must retain the below copyright notice, this list of conditions
//   and the following disclaimer.  
// - Redistributions in binary form must reproduce the above copyright notice, this list of 
//   conditions and the following disclaimer in the documentation and/or other materials provided 
//   with the distribution.  
// - Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
//   promote products derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
// OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
// DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
// IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
// OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// --------------------------------------------------------------------------------------------------
// Author: J Lavanya
// Email id: lavanya.jagan@gmail.com
// -------------------------------------------------------------------------------------------------
//

// -----------------------------------------------------------------------------------------------
// riscv_state_scoreboard
// -----------------------------------------------------------------------------------------------
class riscv_state_scoreboard extends uvm_component;

	// uvm factory registation
	`uvm_component_utils_begin(riscv_state_scoreboard)
	`uvm_component_utils_end

  // data members

	// component members
  virtual riscv_state_if state_if;
  uvm_analysis_export #(riscv_state_seq_item) riscv_state_export; 
  uvm_tlm_analysis_fifo #(riscv_state_seq_item) riscv_state_fifo; 
  
  uvm_analysis_export #(riscv_state_seq_item) spike_state_export; 
  uvm_tlm_analysis_fifo #(riscv_state_seq_item) spike_state_fifo; 
	
  // uvm methods
	extern function new(string name="riscv_state_scoreboard", uvm_component parent);
	extern virtual function void build_phase(uvm_phase phase);
	extern virtual function void connect_phase(uvm_phase phase);
	extern virtual task run_phase(uvm_phase phase);
endclass : riscv_state_scoreboard

// -----------------------------------------------------------------------------------------------
// new
// -----------------------------------------------------------------------------------------------
function riscv_state_scoreboard::new(string name="riscv_state_scoreboard", uvm_component parent);
	super.new(name,parent);
endfunction : new

// -----------------------------------------------------------------------------------------------
// build
// -----------------------------------------------------------------------------------------------
function void riscv_state_scoreboard::build_phase(uvm_phase phase);
	super.build_phase(phase);
  riscv_state_export = new("riscv_state_export", this);
  riscv_state_fifo = new("riscv_state_fifo", this);
  spike_state_export = new("spike_state_export", this);
  spike_state_fifo = new("spike_state_fifo", this);
  if (!uvm_config_db #(virtual riscv_state_if)::get(uvm_root::get(),"*", "state_if",state_if)) begin 
    `uvm_fatal(get_full_name(), $sformatf("Failed to get inst ")) 
  end 

endfunction : build_phase

// -----------------------------------------------------------------------------------------------
// connect
// -----------------------------------------------------------------------------------------------
function void riscv_state_scoreboard::connect_phase(uvm_phase phase);
  riscv_state_export.connect(riscv_state_fifo.analysis_export);
  spike_state_export.connect(spike_state_fifo.analysis_export);
endfunction : connect_phase

// -----------------------------------------------------------------------------------------------
// run
// -----------------------------------------------------------------------------------------------
task riscv_state_scoreboard::run_phase(uvm_phase phase);
  
  riscv_state_seq_item state_pkt;
  riscv_state_seq_item spike_pkt;
  logic [31:0] temp;

  forever begin
    fork 
      // receiving design packet from monitor
      begin
        riscv_state_fifo.get(state_pkt);
        `uvm_info("SHAKTI SB", $sformatf("-----------------"), UVM_LOW)
        //state_pkt.print_riscv_state();
      end
      // receiving model packet from predictor
      begin
        spike_state_fifo.get(spike_pkt);
        `uvm_info("SPIKE SB", $sformatf("-----------------"), UVM_LOW)
        //spike_pkt.print_riscv_state();
      end
    join
    
    // pc check
    if (state_pkt.pc !== spike_pkt.pc) begin
      `uvm_fatal(get_full_name(), $psprintf("PC mismatch, expected:0x%x, observed:0x%x", 
                                           spike_pkt.pc, state_pkt.pc)) 
    end
    // register check
    // floating point register check
    // csr check
    for(temp='h0; temp<spike_pkt.csr_index.size; temp++) begin
      logic [31:0] index;
      index = spike_pkt.csr_index[temp];
      if (state_pkt.csr[index] !== spike_pkt.csr[index]) begin
        `uvm_info("scoreboard", $sformatf("Mismatch %s: expected=0x%x observed=0x%x",
                                          spike_pkt.get_csr_name(index),  
                                          spike_pkt.csr[index], 
                                          state_pkt.csr[index]), UVM_LOW)
      end
   end 

    end
endtask : run_phase

