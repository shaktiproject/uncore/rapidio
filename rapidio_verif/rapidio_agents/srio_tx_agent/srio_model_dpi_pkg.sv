package riscv_spike_dpi_pkg;
  typedef enum
  {
    SPIKE_OK = 0,
    SPIKE_WARN = 1,
    SPIKE_ERROR = 2
  } spike_status_e;

  import "DPI-C" context function chandle SpawnSpikeIF(input string);
  import "DPI-C" context function longint CgetVariable(input chandle, input int);
  import "DPI-C" context function int CsingleStep(input chandle);

endpackage : riscv_spike_dpi_pkg
