/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 

  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: Logical layer Initator request module

--------------------------------------------------------------------------------------------------
*/

package srio_txrx_txblock_ll_ireq;
import DefaultValue ::*;
import srio_txrx_dtypes ::*;

interface Ifc_txblock_ll_ireq;
  method Action data_signals (Ireq_data data_signal); 
  method Action msg_signals (Ireq_msg ireq_msg); 
  
  method Ireq_pkt outputs_initiator_req_pkt ();

 //-- Ready from destination device
//  method Bool srio_tx_link_rdy_n (); 
endinterface : Ifc_txblock_ll_ireq

module mksrio_txrx_txblock_ll_ireq (Ifc_txblock_ll_ireq);

Wire#(Bool) wr_ireq_dest_ready_in <- mkDWire (False);

Wire#(Ireq_data) wr_data_signals <- mkDWire (defaultValue);
Wire#(Ireq_msg) wr_msg_signals <- mkDWire (defaultValue);

Wire#(Ireq_data) wr_init_req_data <- mkDWire (defaultValue);
Wire#(Ireq_msg) wr_init_req_msg <- mkDWire (defaultValue);
Wire#(Ireq_pkt) wr_initiator_req_pkt <- mkDWire (defaultValue);
Reg#(Ireq_pkt) rg_initiator_req_pkt <- mkReg (defaultValue);


rule rl_tx_init_request(wr_data_signals.srio_tx_ireq_ftype == 4'b0010 ||
wr_data_signals.srio_tx_ireq_ftype == 4'b0101 || wr_data_signals.srio_tx_ireq_ftype == 4'b0110);


wr_init_req_data  <=  Ireq_data 
                      {srio_tx_ireq_data : wr_data_signals.srio_tx_ireq_data,
                      srio_tx_ireq_crf : wr_data_signals.srio_tx_ireq_crf,
                      srio_tx_ireq_prio : wr_data_signals.srio_tx_ireq_prio,
                      srio_tx_ireq_ftype : wr_data_signals.srio_tx_ireq_ftype,
                      srio_tx_ireq_tid : wr_data_signals.srio_tx_ireq_tid,
                      srio_tx_ireq_ttype : wr_data_signals.srio_tx_ireq_ttype,                        
                      srio_tx_ireq_addr : wr_data_signals.srio_tx_ireq_addr,
                      srio_tx_ireq_hopcount : wr_data_signals.srio_tx_ireq_hopcount,
                      srio_tx_ireq_byte_count : wr_data_signals.srio_tx_ireq_byte_count,
                      srio_tx_ireq_byte_en_n : wr_data_signals.srio_tx_ireq_byte_en_n,
                      srio_tx_ireq_local: wr_data_signals.srio_tx_ireq_local};

wr_init_req_msg   <=  Ireq_msg
                      {srio_tx_ireq_db_info : wr_msg_signals.srio_tx_ireq_db_info,
                      srio_tx_ireq_msg_len : wr_msg_signals.srio_tx_ireq_msg_len,
                      srio_tx_ireq_msg_seg : wr_msg_signals.srio_tx_ireq_msg_seg,
                      srio_tx_ireq_mbox : wr_msg_signals.srio_tx_ireq_mbox,
                      srio_tx_ireq_letter : wr_msg_signals.srio_tx_ireq_letter};

endrule


rule rl_output_reg;
rg_initiator_req_pkt  <=   Ireq_pkt {ireqdata : wr_init_req_data,
                        ireqmsg : wr_init_req_msg};
endrule


//-- Common Data Signal Ireq Interface
method Action data_signals (Ireq_data data_signal);
   wr_data_signals <= data_signal;
endmethod

//-- Specific Ireq Message Signal Interface
method Action msg_signals (Ireq_msg ireq_msg);
  wr_msg_signals <= ireq_msg;
endmethod


method Ireq_pkt outputs_initiator_req_pkt ();
  return rg_initiator_req_pkt;
  /*return wr_initiator_req_pkt  <=  Ireq_pkt {ireqdata : wr_init_req_data,
                        ireqmsg : wr_init_req_msg};*/

endmethod

endmodule : mksrio_txrx_txblock_ll_ireq
endpackage : srio_txrx_txblock_ll_ireq
