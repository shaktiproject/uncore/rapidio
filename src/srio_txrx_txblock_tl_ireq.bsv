/* 
Copyright (c) 2013, IIT Madras All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this list of conditions
  and the following disclaimer.  
* Redistributions in binary form must reproduce the above copyright notice, this list of 
  conditions and the following disclaimer in the documentation and/or other materials provided 
 with the distribution.  
* Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
  promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS
OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
--------------------------------------------------------------------------------------------------

Author: M Gopinathan
Email id: gopinathan18@gmail.com
Details: Transport layer Initator request module

--------------------------------------------------------------------------------------------------
*/

package srio_txrx_txblock_tl_ireq;

import srio_txrx_dtypes ::*;
import DefaultValue ::*;

interface Ifc_txblock_tl_ireq;
  method Action transport_signals (Ireq_tt tl_signal);
  method Ireq_tt outputs_tl_pkt (); 
endinterface : Ifc_txblock_tl_ireq

module mksrio_txrx_txblock_tl_ireq (Ifc_txblock_tl_ireq);
Wire#(Ireq_tt) wr_tl_signals <- mkDWire (defaultValue);
Reg#(Ireq_tt) rg_tl_pkt <- mkReg (defaultValue);

rule rl_tx_tl;
  rg_tl_pkt <= Ireq_tt  {srio_tx_ireq_tt : wr_tl_signals.srio_tx_ireq_tt,
                         srio_tx_ireq_dest_id : wr_tl_signals.srio_tx_ireq_dest_id,
                         srio_tx_ireq_source_id : wr_tl_signals.srio_tx_ireq_source_id};
endrule


method Action transport_signals (Ireq_tt tl_signal);
  wr_tl_signals <= tl_signal;
endmethod

method Ireq_tt outputs_tl_pkt ();
  return rg_tl_pkt;
endmethod

endmodule : mksrio_txrx_txblock_tl_ireq
endpackage : srio_txrx_txblock_tl_ireq
