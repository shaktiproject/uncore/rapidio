/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- ------------------------------------------------------------------------------------------------------------------------------------------*/

class rapidio_ll_coverage extends uvm_subscriber#(rapidio_ll_sequence_item);

rapidio_ll_sequence_item ltrans;

`uvm_component_utils_begin(rapidio_ll_coverage)
	`uvm_field_object(ltrans,UVM_ALL_ON)
`uvm_component_utils_end

virtual rapidio_interface ll_vif;

//ANALYSIS EXPORTS
 uvm_analysis_imp #(rapidio_ll_sequence_item,rapidio_ll_coverage) dr2cov_ll_export;
 uvm_analysis_imp #(rapidio_ll_sequence_item,rapidio_ll_coverage) mon2cov_ll_export;



function new(string name,uvm_component parent);
super.new(name,parent);
this.ltrans=new();
this.cover_type2_nread_ll_req=new();
this.cover_type2_nread_ll_resp=new();
this.cover_type5_nwrite_ll_req=new();
this.cover_type5_nwrite_ll_resp=new();
this.cover_type6_swrite_ll_req=new();
this.cover_type6_swrite_ll_resp=new();
this.cover_type5_nwrite_r_ll_req=new();
this.cover_type5_nwrite_r_ll_resp=new();
this.cover_type5_atomic_ll_req=new();
this.cover_type5_atomic_ll_resp=new();
this.cover_type2_atomic_ll_req=new();
this.cover_type2_atomic_ll_resp=new();
this.cover_maintanence_write_ll_req=new();
this.cover_maintanence_write_ll_resp=new();
this.cover_maintanence_rell_req=new();
this.cover_maintanence_rell_resp=new();
this.cover_LL_CAR_ll_req=new();
this.cover_LL_CAR_ll_resp=new();
this.cover_LL_CSR_ll_req=new();
this.cover_LL_CSR_ll_resp=new();
this.cover_msg_pkt_req=new();
this.cover_msg_pkt_resp=new();
this.cover_doorbell_pkt_ll_req=new();
this.cover_doorbell_pkt_ll_resp=new();
this.cover_TL_CAR_ll_req=new();
this.cover_TL_CAR_ll_resp=new();
this.cover_valid_dev16_support_ll_req=new();
this.cover_valid_dev16_support_ll_resp=new();
this.cover_valid_dev32_support_ll_req=new();
this.cover_valid_dev32_support_ll_resp=new();

//this.ll_vif;

this.dr2cov_ll_export = new("dr2cov_ll_export",this);
this.mon2cov_ll_export = new("mon2cov_ll_export",this);
endfunction : new

event packet2send_ll_bfm;
event packet2receive_ll_monitor;

virtual function void build_phase(uvm_phase phase);
super.build_phase(phase);
if(!uvm_config_db#(virtual rapidio_interface)::get(this,"","vif",ll_vif))
`uvm_fatal("LL_COVERAGE",$sformatf("CONFIGURATION NOT HAS BEEN SET"))
else
begin
`uvm_info("LL_COVERAGE",$sformatf("CONFIGURATION HAS BEEN SET"),UVM_HIGH)
 $display("the vif reset is =%b",ll_vif.rio_resetn);
 $display("the vif clock is=%b",ll_vif.rio_clk);
end
endfunction : build_phase



//COVERPOINT FOR THE LLTYPE TYPE 2 NREAD REQUEST
covergroup cover_type2_nread_ll_req @(packet2send_ll_bfm )  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type2 = {2};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nread = {4};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE;

endgroup :cover_type2_nread_ll_req

//COVERPOINT FOR THE LLTYPE TYPE 2 NREAD REQUEST

covergroup cover_type2_nread_ll_resp@(packet2receive_ll_monitor )  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type2 = {2};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nread = {4};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE;

endgroup :cover_type2_nread_ll_resp

//NWRITE COVERGROUP REQUEST
covergroup cover_type5_nwrite_ll_req@(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nwrite = {4};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000*[64]};
 //bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa*[1:64]};
 //bins b4 = {32'h55555555*[1:64]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_type5_nwrite_ll_req


//NWRITE RESPONSE
covergroup cover_type5_nwrite_ll_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nwrite = {4};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
 //bins b1 = {32'h0000000[*64]};
 //bins b2 = {32'hffffffff[*64]};
 //bins b3 = {32'haaaaaaaa[*1:64]};
 //bins b4 = {32'h55555555[*1:64]};
 //bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup :cover_type5_nwrite_ll_resp


//PACKET TYPE 6  SWRITE REQUEST COVERGROUP 
covergroup cover_type6_swrite_ll_req@(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type = {5};
 bins bin_ftypes = default;
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

endgroup : cover_type6_swrite_ll_req


//PACKET TYPE 6  SWRITE RESPONSE COVERGROUP 

covergroup cover_type6_swrite_ll_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type = {5};
 bins bin_ftypes = default;
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

endgroup : cover_type6_swrite_ll_resp

//PACKET TYPE 5  NWRITE_R REQUEST COVERGROUP 
covergroup cover_type5_nwrite_r_ll_req @(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nwrite = {5};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint data {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup :cover_type5_nwrite_r_ll_req

//PACKET TYPE 5  NWRITE_R RESPONSE COVERGROUP 
covergroup cover_type5_nwrite_r_ll_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_nwrite = {5};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:15]};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint data {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_type5_nwrite_r_ll_resp

//PACKET TYPE 5  ATOMIC REQUEST COVERGROUP
covergroup cover_type5_atomic_ll_req @(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_swap = {4'b1100};
 bins bin_compare_n_swap = {4'b1101};
 bins bin_test_n_swap = {4'b1110};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:4],6,8};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*1:2]};
// bins b2 = {32'hffffffff[*1:2]};
// bins b3 = {32'haaaaaaaa[*1:2]};
// bins b4 = {32'h55555555[*1:2]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_type5_atomic_ll_req


//PACKET TYPE 5  ATOMIC RESPONSE COVERGROUP
covergroup cover_type5_atomic_ll_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type5 = {5};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_swap = {4'b1100};
 bins bin_compare_n_swap = {4'b1101};
 bins bin_test_n_swap = {4'b1110};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {[0:4],6,8};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*1:2]};
// bins b2 = {32'hffffffff[*1:2]};
// bins b3 = {32'haaaaaaaa[*1:2]};
// bins b4 = {32'h55555555[*1:2]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_type5_atomic_ll_resp


//PACKET TYPE 2  ATOMIC REQUEST COVERGROUP
covergroup cover_type2_atomic_ll_req@(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type2 = {2};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_incr = {4'b1100};
 bins bin_decr = {4'b1101};
 bins bin_set = {4'b1101};
 bins bin_clr = {4'b1111};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {[0:4],6,8};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE ;

endgroup : cover_type2_atomic_ll_req

//PACKET TYPE 2  ATOMIC REQUEST COVERGROUP
covergroup cover_type2_atomic_ll_resp@(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type2 = {2};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_incr = {4'b1100};
 bins bin_decr = {4'b1101};
 bins bin_set = {4'b1101};
 bins bin_clr = {4'b1111};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {[0:4],6,8};
}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

ADDRESS : coverpoint ltrans.addr {
 bins b1 = {29'h00000000};
 bins b2 = {29'h3fffffff};
 bins b3 = {29'h00000010,29'h 0000ff00,29'h0000ff10,29'h00ff1000,29'hff100000} ;
 bins b4 = default;
}

EXT_ADDR : coverpoint ltrans.ext_addr {
 bins b1 = {32'h00000000};
 bins b2 = {32'h0000ff00};
 bins b3 = {32'h0000ff10};
 bins b4 = {32'h0000ffff};
 bins b5 = {32'hffff0000};
 bins b6 = {32'hfffff100};
 bins b7 = {32'hffffffff};
 bins b8 = default;
}

XAMBS : coverpoint ltrans.xamsbs {
 bins b1 = {2'b00};
 bins b2 = {2'b01};
 bins b3 = {2'b10};
 bins b4 = {2'b11};
}

CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE ;

endgroup : cover_type2_atomic_ll_resp

//TYPE8 MAINTANANCE WRITE REQUEST COVER GROUP
covergroup cover_maintanence_write_ll_req@(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mwrite = {4'b0001};
 bins bin_m_postwrite = {4'b0100};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
 bins bin_wrsize[] = {4'b1000,4'b1011,4'b1100};
 ignore_bins bin_illegal = {[4'b0001:4'b0111],[4'b1000:4'b1010],[4'b1101:4'b1111]};

}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

CONFIG_OFFSET : coverpoint ltrans.config_offset {
 bins b1 = {21'h000000};
 bins b2 = {21'h1fffff};
 bins b3 = {21'h000010,21'h 00ff00,21'h00ff10,21'hff1000} ;
 bins b4 = default;
}


//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_maintanence_write_ll_req

//TYPE8 MAINTANANCE WRITE REQUEST COVER GROUP

covergroup cover_maintanence_write_ll_resp@(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mwrite = {4'b0001};
 bins bin_m_postwrite = {4'b0100};
 bins bin_ttypes = default;
}

WRSIZE : coverpoint ltrans.wrsize {
bins bin_wrsize[] = {4'b1000,4'b1011,4'b1100};
 ignore_bins bin_illegal = {[4'b0001:4'b0111],[4'b1000:4'b1010],[4'b1101:4'b1111]};

}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

CONFIG_OFFSET : coverpoint ltrans.config_offset {
 bins b1 = {21'h000000};
 bins b2 = {21'h1fffff};
 bins b3 = {21'h000010,21'h 00ff00,21'h00ff10,21'hff1000} ;
 bins b4 = default;
}


//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

CROSS_WDPTR_WRSIZE : cross WDPTR, WRSIZE ;

endgroup : cover_maintanence_write_ll_resp


//TYPE8 MAINTANANCE READ REQUEST COVER GROUP
covergroup cover_maintanence_read_ll_req @(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mread = {4'b0000};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {4'b1000,4'b1011,4'b1100};
 illegal_bins bin_illegal = {[4'b000:4'b0111],[4'b1000:4'b1010],[4'b1101:4'b1111]};

}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

CONFIG_OFFSET : coverpoint ltrans.config_offset {
 bins b1 = {21'h000000};
 bins b2 = {21'h1fffff};
 bins b3 = {21'h000010,21'h 00ff00,21'h00ff10,21'hff1000} ;
 bins b4 = default;
}


CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE ;

endgroup  : cover_maintanence_read_ll_req


//TYPE8 MAINTANANCE READ REQUEST COVER GROUP
covergroup cover_maintanence_read_ll_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mread = {4'b0000};
 bins bin_ttypes = default;
}

RDSIZE : coverpoint ltrans.rdsize {
 bins bin_rdsize[] = {4'b1000,4'b1011,4'b1100};
 illegal_bins bin_illegal = {[4'b000:4'b0111],[4'b1000:4'b1010],[4'b1101:4'b1111]};

}

WDPTR : coverpoint ltrans.wdptr {
 bins wdptr_0 = {0};
 bins Wdpr_1 = {1};
}

CONFIG_OFFSET : coverpoint ltrans.config_offset {
 bins b1 = {21'h000000};
 bins b2 = {21'h1fffff};
 bins b3 = {21'h000010,21'h 00ff00,21'h00ff10,21'hff1000} ;
 bins b4 = default;
}
CROSS_WDPTR_RDSIZE : cross WDPTR, RDSIZE ;

endgroup  : cover_maintanence_read_ll_resp

//MAINTANANCE RESPONSE PACKET 
covergroup cover_maintanence_rell_req @(packet2send_ll_bfm)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mread_resp = {4'b0010};
 bins bin_mwrite_resp = {4'b0011};
 bins bin_ttypes = default;
}

STATUS : coverpoint ltrans.status {
 bins bin_done = {4'b0000};
 bins bin_error = {4'b0111};
 ignore_bins bin_ignore = {[4'b0001:4'b0110],[4'b1000:4'b1111]};
}


//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

endgroup : cover_maintanence_rell_req

//MAINTANANCE RESPONSE PACKET 
covergroup cover_maintanence_rell_resp @(packet2receive_ll_monitor)  ; 
 
FTYPE : coverpoint ltrans.ftype {
 bins bin_type8 = {8};
 bins bin_ftypes = default;
}

TRANSACTION : coverpoint ltrans.ttype {
 bins bin_mread_resp = {4'b0010};
 bins bin_mwrite_resp = {4'b0011};
 bins bin_ttypes = default;
}

STATUS : coverpoint ltrans.status {
 bins bin_done = {4'b0000};
 bins bin_error = {4'b0111};
 ignore_bins bin_ignore = {[4'b0001:4'b0110],[4'b1000:4'b1111]};
}

//DATA : coverpoint ltrans.lldata {
// bins b1 = {32'h0000000[*64]};
// bins b2 = {32'hffffffff[*64]};
// bins b3 = {32'haaaaaaaa[*1:64]};
// bins b4 = {32'h55555555[*1:64]};
// bins b5 = default;
//}

endgroup : cover_maintanence_rell_resp



//REMAINING COVERGROUPS

//COVERGROUP : cover_LL_CAR_ll_bfm
covergroup cover_LL_CAR_ll_req @(packet2send_ll_bfm)  ; 
 
ADDR_CAR : coverpoint ltrans.addr {
 bins b1 = {8'h0,32'h4,8'h8,8'hc,8'h10,8'h14,8'h18,8'h1c};
 bins b2 = {[16'h20:16'h48]};
 bins b3 = {[16'h100:16'hfffc]};
 bins b4 = {[24'h10000:24'hfffffc]};
}

//DATA_CAR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CAR : cross ADDR_CAR, DATA_CAR ;

endgroup : cover_LL_CAR_ll_req

//COVERGROUP : cover_LL_CAR_ll_req
covergroup cover_LL_CAR_ll_resp @(packet2receive_ll_monitor)  ; 
 
ADDR_CAR : coverpoint ltrans.addr {
 bins b1 = {8'h0,32'h4,8'h8,8'hc,8'h10,8'h14,8'h18,8'h1c};
 bins b2 = {[16'h20:16'h48]};
 bins b3 = {[16'h100:16'hfffc]};
 bins b4 = {[24'h10000:24'hfffffc]};
}

//DATA_CAR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CAR : cross ADDR_CAR, DATA_CAR ;

endgroup : cover_LL_CAR_ll_resp


//COVERGROUP : cover_LL_CSR_ll_req
covergroup cover_LL_CSR_ll_req @(packet2send_ll_bfm)  ; 
 
ADDR_CSR : coverpoint ltrans.addr {
 bins b1 = {8'h4c,8'h58,8'h5c};
 bins b2 = {8'h50,[8'h60:8'hfc]};
}

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CSR : cross ADDR_CSR, DATA_CSR ;

endgroup

//COVERGROUP : cover_LL_CSR_ll_req
covergroup cover_LL_CSR_ll_resp @(packet2receive_ll_monitor)  ; 
 
ADDR_CSR : coverpoint ltrans.addr {
 bins b1 = {8'h4c,8'h58,8'h5c};
 bins b2 = {8'h50,[8'h60:8'hfc]};
}

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CSR : cross ADDR_CSR, DATA_CSR ;

endgroup : cover_LL_CSR_ll_resp


//COVERGROUP : cover_msg_pkt REQUEST 
covergroup cover_msg_pkt_req@(packet2send_ll_bfm)  ; 
 
//option.name = "Message Data coverage" ;
//PKT_NUM : coverpoint pkt_count {
// bins count[] = {[1:16]};
//}

//PAYLOAD : coverpoint DATA_PAYLOAD {
// bins one = {1};
// bins max = {256};
//}

FORMAT_TYPE : coverpoint ltrans.ftype {
 bins Type_11 = {11};
}

endgroup : cover_msg_pkt_req

//COVERGROUP : cover_msg_pkt REQUEST 
covergroup cover_msg_pkt_resp@(packet2receive_ll_monitor)  ; 
 
//option.name = "Message Data coverage" ;
//PKT_NUM : coverpoint pkt_count {
// bins count[] = {[1:16]};
//}

//PAYLOAD : coverpoint DATA_PAYLOAD {
// bins one = {1};
// bins max = {256};
//}

FORMAT_TYPE : coverpoint ltrans.ftype {
 bins Type_11 = {11};
}

endgroup : cover_msg_pkt_resp


//COVERGROUP : cover_doorbell_pkt_REQ
covergroup cover_doorbell_pkt_ll_req @(packet2send_ll_bfm)  ; 
 
FORMAT_TYPE : coverpoint ltrans.ftype {
 bins Type_10 = {10};
}

endgroup : cover_doorbell_pkt_ll_req


//COVERGROUP : cover_doorbell_pkt_RESP
covergroup cover_doorbell_pkt_ll_resp @(packet2receive_ll_monitor)  ; 
 
FORMAT_TYPE : coverpoint ltrans.ftype {
 bins Type_10 = {10};
}

endgroup : cover_doorbell_pkt_ll_resp


//cover_TL_CAR_ll_req REQUEST 
covergroup cover_TL_CAR_ll_req @(packet2send_ll_bfm)  ; 
 
//option.goal = 100;

ADDR_CAR : coverpoint ltrans.addr {
 bins b1 = {32'h0,32'h4,32'h8,32'hc};
 bins b2 = {32'h10};
 bins b3 = {32'h14, 32'h18, 32'h1c, 32'h20, 32'h24, 32'h28, 32'h2c,32'h30};
 bins b4 = {32'h34};
 bins b5 = {32'h38,32'h3c};
}

//DATA_CAR : coverpoint ltrans.lldata {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}
//
//ADDR_DATA_CAR : cross ADDR_CAR, DATA_CAR ;

endgroup : cover_TL_CAR_ll_req


//cover_TL_CAR_ll_req REQUEST 
covergroup cover_TL_CAR_ll_resp @(packet2receive_ll_monitor)  ; 
 
//option.goal = 100;

ADDR_CAR : coverpoint ltrans.addr {
 bins b1 = {32'h0,32'h4,32'h8,32'hc};
 bins b2 = {32'h10};
 bins b3 = {32'h14, 32'h18, 32'h1c, 32'h20, 32'h24, 32'h28, 32'h2c,32'h30};
 bins b4 = {32'h34};
 bins b5 = {32'h38,32'h3c};
}

//DATA_CAR : coverpoint ltrans.lldata {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}
//
//ADDR_DATA_CAR : cross ADDR_CAR, DATA_CAR ;

endgroup : cover_TL_CAR_ll_resp



//COVERGROUP : cover_TL_CSR_ll_req
covergroup cover_TL_CSR_ll_req @(packet2send_ll_bfm)  ; 
 
option.goal = 100;

ADDR_CSR : coverpoint ltrans.addr {
 bins b1 = {32'h40,32'h44,32'h48,32'h4c,32'h50,32'h54,32'h58,32'h5c};
 bins b2 = {32'h60,32'h64,32'h6c,32'h70,32'h74,32'h78};
 bins b3 = {32'h7c};
 bins b4 = {[32'h80:32'h21c]};
 bins b5 = {32'h220};
 bins b6 = {32'h224,32'h22c};
 bins b7 = {32'h230,32'h234,32'h238};
 bins b8 = {32'h23c};
}

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CSR : cross ADDR_CSR, DATA_CSR ;

endgroup : cover_TL_CSR_ll_req

//endgroup : cover_valid_dev16_support_ll_req


//COVERGROUP : cover_TL_CSR_ll_req
covergroup cover_TL_CSR_ll_resp @(packet2receive_ll_monitor)  ; 
 
//option.goal = 100;

ADDR_CSR : coverpoint ltrans.addr {
 bins b1 = {32'h40,32'h44,32'h48,32'h4c,32'h50,32'h54,32'h58,32'h5c};
 bins b2 = {32'h60,32'h64,32'h6c,32'h70,32'h74,32'h78};
 bins b3 = {32'h7c};
 bins b4 = {[32'h80:32'h21c]};
 bins b5 = {32'h220};
 bins b6 = {32'h224,32'h22c};
 bins b7 = {32'h230,32'h234,32'h238};
 bins b8 = {32'h23c};
}

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//ADDR_DATA_CSR : cross ADDR_CSR, DATA_CSR ;

endgroup : cover_TL_CSR_ll_resp


//COVERGROUP : cover_valid_dev16_support_ll_req
covergroup cover_valid_dev16_support_ll_req @(packet2send_ll_bfm)  ; 
 
ADDR_CAR : coverpoint ltrans.addr[27] ;

//DATA_CSR : coverpoint ltrans.lldata[31:16] {
// bins b1 = {0000_0000};
// bins b2 = {0101_0101};
// bins b3 = {1010_1010};
// bins b4 = {1111_1111};
//}

//VALID_DEV16_SUPPORT : cross ADDR_CAR, DATA_CSR ;

endgroup : cover_valid_dev16_support_ll_req


//COVERGROUP : cover_valid_dev32_support_ll_req
covergroup cover_valid_dev32_support_ll_req @(packet2send_ll_bfm)  ; 
 
//option.goal = 100;

ADDR_CAR : coverpoint ltrans.addr[19] ;

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//VALID_DEV32_SUPPORT : cross ADDR_CAR, DATA_CSR ;

endgroup:cover_valid_dev32_support_ll_req


covergroup cover_valid_dev16_support_ll_resp@(packet2receive_ll_monitor)  ; 
 
ADDR_CAR : coverpoint ltrans.addr[27] ;

//DATA_CSR : coverpoint data[31:16] {
// bins b1 = {0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111};
//}
//
//VALID_DEV16_SUPPORT : cross {ADDR_CAR, DATA_CSR} ;

endgroup : cover_valid_dev16_support_ll_resp


//COVERGROUP : cover_valid_dev32_support_ll_req
covergroup cover_valid_dev32_support_ll_resp @(packet2receive_ll_monitor)  ; 
 
//option.goal = 100;

ADDR_CAR : coverpoint ltrans.addr[19] ;

//DATA_CSR : coverpoint data {
// bins b1 = {0000_0000_0000_0000_0000_0000_0000_0000};
// bins b2 = {0101_0101_0101_0101_0101_0101_0101_0101};
// bins b3 = {1010_1010_1010_1010_1010_1010_1010_1010};
// bins b4 = {1111_1111_1111_1111_1111_1111_1111_1111};
//}

//VALID_DEV32_SUPPORT : cross ADDR_CAR, DATA_CSR ;

endgroup:cover_valid_dev32_support_ll_resp




// Implementation of an write funciton //
virtual function void write(rapidio_ll_sequence_item t);
this.ltrans = t;
this.cover_type2_nread_ll_req.sample();
this.cover_type2_nread_ll_resp.sample();
this.cover_type5_nwrite_ll_req.sample();
this.cover_type5_nwrite_ll_resp.sample();
this.cover_type6_swrite_ll_req.sample();
this.cover_type6_swrite_ll_resp.sample();
this.cover_type5_nwrite_r_ll_req.sample();
this.cover_type5_nwrite_r_ll_resp.sample();
this.cover_type5_atomic_ll_req.sample();
this.cover_type5_atomic_ll_resp.sample();
this.cover_type2_atomic_ll_req.sample();
this.cover_type2_atomic_ll_resp.sample();
this.cover_maintanence_write_ll_req.sample();
this.cover_maintanence_write_ll_resp.sample();
this.cover_maintanence_read_ll_req.sample();
this.cover_maintanence_read_ll_resp.sample();
this.cover_maintanence_rell_resp.sample();
this.cover_maintanence_rell_req.sample();

this.cover_LL_CAR_ll_req.sample();
this.cover_LL_CAR_ll_resp.sample();
this.cover_LL_CSR_ll_req.sample();
this.cover_LL_CSR_ll_resp.sample();
this.cover_msg_pkt_req.sample();
this.cover_msg_pkt_resp.sample();
this.cover_doorbell_pkt_ll_req.sample();
this.cover_doorbell_pkt_ll_resp.sample();
this.cover_TL_CAR_ll_req.sample();
this.cover_TL_CAR_ll_resp.sample();
this.cover_valid_dev16_support_ll_req.sample();
this.cover_valid_dev16_support_ll_resp.sample();
this.cover_valid_dev32_support_ll_req.sample();
this.cover_valid_dev32_support_ll_resp.sample();
endfunction 



endclass : rapidio_ll_coverage


