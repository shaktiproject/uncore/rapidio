/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_PACKET_SVH
`define RAPIDIO_PACKET_SVH

// CLASS: rapidio_sequence_item
class rapidio_ll_sequence_item extends uvm_sequence_item;

  typedef enum bit [4:0] { NREAD,ATOMIC_INC,ATOMIC_DEC,ATOMIC_SET,ATOMIC_CLEAR,NWRITE,NWRITE_R,ATOMIC_SWAP,ATOMIC_COMPARE_N_SWAP,ATOMIC_TEST_N_SWAP,MAINTENANCE_READ_REQ,MAINTENANCE_WRITE_REQ,MAINTENANCE_READ_RESP,MAINTENANCE_WRITE_RESP,MAINTENANCE_PORT_WRITE_REQ,RESPONSE_WITH_PYLD,RESPONSE_WITH_NO_PYLD,SWRITE } rapidio_oprt;


 

  // RAPIDIO Signals
   rand bit [63:0]  data [];
   rand bit [63:0]  data_item ;
   rand bit [3:0]   ftype; 
   rand bit [3:0]   wrsize; 
   rand bit [3:0]   rdsize; 
   rand bit         wdptr; 
   rand bit [7:0]   src_tid;
   rand bit [7:0]   dest_tid;
   rand bit [28:0]  addr;
   rand bit [3:0]   status;
   rand bit [31:0]  ext_addr;
   rand bit [1:0]   xamsbs;
   rand bit [3:0]   ttype;  
   rand bit [15:0]  db_info;
   rand bit [3:0]   msg_len;
   rand bit [3:0]   msg_seg;
   rand bit [5:0]   mbox;
   rand bit [1:0]   letter;
   rand bit [2047:0] byte_count_c;
   rand bit [2047:0] wr_byte_count_c;
   rand bit [2047:0] rd_dword_count;
   rand bit [2047:0] wr_dword_count;
   rand bit [20:0] config_offset;
   rand rapidio_oprt kind ;
   int  dword_length;
   bit select;
   logic [31:0] ral_data;

  

  // For ERROR INJECTION 
   

  // UVM automation macros
  `uvm_object_utils_begin(rapidio_ll_sequence_item)
     `uvm_field_array_int(data, UVM_ALL_ON | UVM_NOPACK);
     `uvm_field_int (ftype,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (wrsize,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (rdsize,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (wdptr,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (src_tid,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (dest_tid,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (ext_addr,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (addr,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (status,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (xamsbs,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (ttype,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (db_info,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (msg_len,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (msg_seg,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (mbox,UVM_ALL_ON|UVM_NOPACK);
     `uvm_field_int (letter,UVM_ALL_ON|UVM_NOPACK);


  `uvm_object_utils_end



function void do_pack ( uvm_packer packer);
     string trans_type ;
     bit [3:0] bit_padding = 4'b0000;
     bit [23:0] reserved_bits = 24'd0;
     super.do_pack ( packer);
     trans_type = get_transaction_type (ftype ,ttype);
     `uvm_info("RAPIDIO_LL_SEQ_ITEM_INFO",$sformatf("RAPIDIO_LL_SEQ_ITEM of %s\n",trans_type),UVM_LOW)
     if ( (trans_type == "NREAD") )
     begin
        packer.pack_field_int (ftype, $bits(ftype));
        packer.pack_field_int (ttype, $bits(ttype));
        packer.pack_field_int(rdsize, $bits(rdsize));
        packer.pack_field_int(src_tid, $bits(src_tid));
        packer.pack_field_int(addr,   $bits(addr));
        packer.pack_field_int(wdptr,  $bits(wdptr));
        packer.pack_field_int(xamsbs, $bits(xamsbs));
        packer.pack_field_int(bit_padding, $bits(bit_padding));

     end
     else if ( trans_type == "NWRITE")
     begin
        packer.pack_field_int (ftype, $bits(ftype));
        packer.pack_field_int (ttype, $bits(ttype));
        packer.pack_field_int(wrsize, $bits(wrsize));
        packer.pack_field_int(src_tid, $bits(src_tid));
        packer.pack_field_int(addr,   $bits(addr));
        packer.pack_field_int(wdptr,  $bits(wdptr));
        packer.pack_field_int(xamsbs, $bits(xamsbs));
        foreach(data[i]) packer.pack_field_int(data[i],64);
        packer.pack_field_int(bit_padding, $bits(bit_padding));
     end
     else if ( trans_type == "NWRITE_R")
     begin
        packer.pack_field_int (ftype, $bits(ftype));
        packer.pack_field_int (ttype, $bits(ttype));
        packer.pack_field_int(wrsize, $bits(wrsize));
        packer.pack_field_int(src_tid, $bits(src_tid));
        packer.pack_field_int(addr,   $bits(addr));
        packer.pack_field_int(wdptr,  $bits(wdptr));
        packer.pack_field_int(xamsbs, $bits(xamsbs));
        foreach(data[i]) packer.pack_field_int(data[i],64);
        packer.pack_field_int(bit_padding, $bits(bit_padding));
     end
     else if ( trans_type == "RESPONSE_WITH_PYLD")
     begin
        packer.pack_field_int (ftype, $bits(ftype));
        packer.pack_field_int (ttype, $bits(ttype));
        packer.pack_field_int(status, $bits(status));
        packer.pack_field_int(dest_tid, $bits(src_tid));
     //`uvm_info("RAPIDIO_LL_SEQ_ITEM_INFO",$sformatf("RAPIDIO_LL_SEQ_ITEM DWORD of %d ,data_size =%d \n",rd_dword_count,data.size),UVM_LOW)
        foreach(data[i])  begin 
               packer.pack_field_int(data[i],64);
              //`uvm_info("RAPIDIO_LL_SEQ_ITEM_INFO",$sformatf("LL_SEQ_ITEM foreach  %d ,data_size =%d \n",i,data.size),UVM_LOW)
        end
        packer.pack_field_int(bit_padding, $bits(bit_padding));
     end
     else if ( trans_type == "RESPONSE_WITH_NO_PYLD")
     begin
        packer.pack_field_int (ftype, $bits(ftype));
        packer.pack_field_int (ttype, $bits(ttype));
        packer.pack_field_int(status, $bits(status));
        packer.pack_field_int(dest_tid, $bits(src_tid));
        packer.pack_field_int(bit_padding, $bits(bit_padding));
     end
   

  endfunction: do_pack


function string get_transaction_type ( bit [3:0] ftype, bit [3:0] ttype);
    string xaction_type;
    if ( ftype == 4'h2 && ttype == 4'h4) xaction_type = "NREAD";
    else if ( ftype == 4'h2 && ttype == 4'hc) xaction_type = "ATOMIC_INC"; 
    else if ( ftype == 4'h2 && ttype == 4'hd) xaction_type = "ATOMIC_DEC"; 
    else if ( ftype == 4'h2 && ttype == 4'he) xaction_type = "ATOMIC_SET"; 
    else if ( ftype == 4'h2 && ttype == 4'hf) xaction_type = "ATOMIC_CLEAR"; 
    else if ( ftype == 4'h5 && ttype == 4'h4) xaction_type = "NWRITE"; 
    else if ( ftype == 4'h5 && ttype == 4'h5) xaction_type = "NWRITE_R"; 
    else if ( ftype == 4'h5 && ttype == 4'hc) xaction_type = "ATOMIC_SWAP"; 
    else if ( ftype == 4'h5 && ttype == 4'hd) xaction_type = "ATOMIC_COMPARE_N_SWAP"; 
    else if ( ftype == 4'h5 && ttype == 4'he) xaction_type = "ATOMIC_TEST_N_SWAP"; 
    else if ( ftype == 4'd8 && ttype == 4'h0)xaction_type = "MAINTENANCE_READ_REQ"; 
    else if ( ftype == 4'd8 && ttype == 4'h1)xaction_type = "MAINTENANCE_WRITE_REQ"; 
    else if ( ftype == 4'd8 && ttype == 4'h2)xaction_type = "MAINTENANCE_READ_RESP"; 
    else if ( ftype == 4'd8 && ttype == 4'h3)xaction_type = "MAINTENANCE_WRITE_RESP"; 
    else if ( ftype == 4'd8 && ttype == 4'h4)xaction_type = "MAINTENANCE_PORT_WRITE_REQ"; 
    else if ( ftype == 4'd13 && ttype == 4'h0)xaction_type = "RESPONSE_WITH_NO_PYLD"; 
    else if ( ftype == 4'd13 && ttype == 4'h8)xaction_type = "RESPONSE_WITH_PYLD"; 
    else if ( ftype == 4'd6 )xaction_type = "SWRITE"; 
    else xaction_type = "RESERVED";


 return xaction_type; 

endfunction:get_transaction_type



function  void do_unpack( uvm_packer packer);
   string trans_type ;
   bit [3:0] bit_padding ;
   super.do_unpack(packer);
   ftype = packer.unpack_field_int ($bits(ftype));
   if (ftype != 4'd6) ttype =  packer.unpack_field_int ($bits(ttype));
   trans_type = get_transaction_type (ftype ,ttype);
   if ( ftype == 4'd2) // TYPE2
   begin
      rdsize =  packer.unpack_field_int ($bits(rdsize));
      src_tid =  packer.unpack_field_int ($bits(src_tid));
      addr =    packer.unpack_field_int ($bits(addr));
      wdptr =   packer.unpack_field_int ($bits(wdptr));
      xamsbs =  packer.unpack_field_int ($bits(xamsbs));
   end
   else if ( ftype == 4'd5) // TYPE5
 //  else if ( trans_type == "NWRITE")
   begin
      dword_length = (dword_length -3)/ 8;
      wrsize =  packer.unpack_field_int ($bits(wrsize));
      src_tid =  packer.unpack_field_int ($bits(src_tid));
      addr =    packer.unpack_field_int ($bits(addr));
      wdptr =   packer.unpack_field_int ($bits(wdptr));
      xamsbs =  packer.unpack_field_int ($bits(xamsbs));
      data.delete();
      data = new[dword_length];
      for ( int i = 0 ; i < dword_length; i++)  begin
        data[i] =  packer.unpack_field_int(64);
      end
   end
   else if ( trans_type == "RESPONSE_WITH_PYLD")
   begin
      dword_length = (dword_length -3)/ 8;
      status =    packer.unpack_field_int ($bits(status));
      dest_tid =  packer.unpack_field_int ($bits(dest_tid));
      data.delete();
      data = new[dword_length];
      //`uvm_info("RAPIDIO_LL_SEQ_ITEM_INFO",$sformatf("RAPIDIO_LL_SEQ_UNPACK of %d\n",dword_length),UVM_LOW)
     for ( int i = 0 ; i < dword_length; i++)  begin
        data[i] =  packer.unpack_field_int(64);
     end
     
   end
   else if ( trans_type == "RESPONSE_WITH_NO_PYLD")
   begin
      status =    packer.unpack_field_int ($bits(status));
      dest_tid =  packer.unpack_field_int ($bits(dest_tid));
   end

   

endfunction: do_unpack




  // FUNC : Constructor
  function new (string name = "rapidio_ll_sequence_item");
    super.new(name);
  endfunction : new

  // Constraints
  // constraint payload_size_c {data.size inside {[1:32]};}

   constraint wdptr1 { wdptr inside {0,1}; }
   
   constraint rdsize1 { rdsize inside { [1:15] }; }

   constraint wrsize1 { wrsize inside { [1:15] }; }


  constraint wdptr_rdsize_data_256 { if ( (wdptr == 1'b1) && (rdsize == 4'b1111) )
    			 { byte_count_c inside {[8:256] };} }
  
   constraint wdptr_rdsize_data_224 { if ( (wdptr == 1'b0) && (rdsize == 4'b1111) )
    			 { byte_count_c inside {[8:224] };} }
  
   constraint wdptr_rdsize_data_192 { if ( (wdptr == 1'b1) && (rdsize == 4'b1110) )
    			 { byte_count_c inside {[8:224] };} }
  
   constraint wdptr_rdsize_data_160 { if ( (wdptr == 1'b0) && (rdsize == 4'b1110) )
    			 { byte_count_c inside {[8:160] };} }

   constraint wdptr_rdsize_data_128 { if ( (wdptr == 1'b1) && (rdsize == 4'b1101) )
    			 { byte_count_c inside {[8:128] };} }

   constraint wdptr_rdsize_data_96 { if ( (wdptr == 1'b0) && (rdsize == 4'b1101) )
    			 { byte_count_c inside {[8:96] };} }
  
   constraint wdptr_rdsize_data_64 { if ( (wdptr == 1'b1) && (rdsize == 4'b1100) )
    			 { byte_count_c inside {[8:64] };} }

   constraint wdptr_rdsize_data_32 { if ( (wdptr == 1'b0) && (rdsize == 4'b1100) )
    			 { byte_count_c inside {[8:32 ]};} }
  
   constraint wdptr_rdsize_data_16 { if ( (wdptr == 1'b1) && (rdsize == 4'b1011) )
    			 { byte_count_c inside {[8:16] };} }

   constraint wdptr_rdsize_data_8 { if ( (wdptr == 1'b0) && (rdsize == 4'b1011) )
    			 { byte_count_c inside {[8:8] };} }
  
   constraint wdptr_rdsize_data_below_8 { if (  rdsize < 4'b1011 )
    			 { byte_count_c inside {[8:8] };} }

   constraint dword_count_c { rd_dword_count == (byte_count_c/8) ;  }

   //constraint payload_size_c {data.size < rd_dword_count; data.size >0; }


  constraint wdptr_wrsize_data_256 { if ( (wdptr == 1'b1) && (wrsize == 4'b1111) )
    			 { wr_byte_count_c inside {[8:256] };} }
  
   constraint wdptr_wrsize_data_224 { if ( (wdptr == 1'b0) && (wrsize == 4'b1111) )
    			 { wr_byte_count_c inside {[8:224] };} }
  
   constraint wdptr_wrsize_data_192 { if ( (wdptr == 1'b1) && (wrsize == 4'b1110) )
    			 { wr_byte_count_c inside {[8:224] };} }
  
   constraint wdptr_wrsize_data_160 { if ( (wdptr == 1'b0) && (wrsize == 4'b1110) )
    			 { wr_byte_count_c inside {[8:160] };} }

   constraint wdptr_wrsize_data_128 { if ( (wdptr == 1'b1) && (wrsize == 4'b1101) )
    			 { wr_byte_count_c inside {[8:128] };} }

   constraint wdptr_wrsize_data_96 { if ( (wdptr == 1'b0) && (wrsize == 4'b1101) )
    			 { wr_byte_count_c inside {[8:96] };} }
  
   constraint wdptr_wrsize_data_64 { if ( (wdptr == 1'b1) && (wrsize == 4'b1100) )
    			 { wr_byte_count_c inside {[8:64] };} }

   constraint wdptr_wrsize_data_32 { if ( (wdptr == 1'b0) && (wrsize == 4'b1100) )
    			 { wr_byte_count_c inside {[8:32 ]};} }
  
   constraint wdptr_wrsize_data_16 { if ( (wdptr == 1'b1) && (wrsize == 4'b1011) )
    			 { wr_byte_count_c inside {[8:16] };} }

   constraint wdptr_wsize_data_8 { if ( (wdptr == 1'b0) && (wrsize == 4'b1011) )
    			 { wr_byte_count_c inside {[8:8] };} }
  
   constraint wdptr_wrsize_data_below_8 { if (  wrsize < 4'b1011 )
    			 { wr_byte_count_c inside {[8:8] };} }

   constraint wr_dword_count_c { wr_dword_count == (wr_byte_count_c/8) ;  }

endclass : rapidio_ll_sequence_item 


// CLASS: rapidio_sequence_item_error
class rapidio_ll_sequence_item_error extends rapidio_ll_sequence_item;

  // UVM automation macros
  `uvm_object_utils(rapidio_ll_sequence_item_error);

  // FUNC : Constructor
  function new(string name = "rapidio_ll_sequence_item_error");
    super.new(name);
  endfunction : new

  // Constraints


endclass : rapidio_ll_sequence_item_error


// CLASS: ahb_sequence_item_top
class rapidio_ll_sequence_item_top extends uvm_sequence_item;
  rapidio_ll_sequence_item 	rapidio_ll_sequence_item_prev;
  rapidio_ll_sequence_item 	rapidio_ll_sequence_item_curr;
 
  // UVM automation macros
  `uvm_object_utils_begin (rapidio_ll_sequence_item_top)
    `uvm_field_object(rapidio_ll_sequence_item_prev, UVM_ALL_ON|UVM_NOPACK);
    `uvm_field_object(rapidio_ll_sequence_item_curr, UVM_ALL_ON|UVM_NOPACK);
  `uvm_object_utils_end

  // FUNC : Constructor
  function new(string name = "rapidio_transfer_inst1");
    super.new(name);
  endfunction : new



endclass : rapidio_ll_sequence_item_top


`endif
