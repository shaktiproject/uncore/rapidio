/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_IOPkt_concatenation module and the RapidIO_IOPkt_Generation module for ftype 11 Message Passing.
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module and the RapidIO_IOPkt_Generation module for message passing operation. 
--
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_MessagePassing;

import DefaultValue ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;

module mkTb_for_MessagePassing(Empty);

Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;


// For Concatenation Module

Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);

Wire#(FType11_MESSAGEClass) wr_ireq_FType11_MESSAGEClass <- mkDWire (defaultValue);

Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module

Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False); 



// Clock Declaration 
Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 5)
	$finish (0);
endrule

// -------------------Initiator request ----------------- Ftype 11 ---------- MESSAGEClass ------------ //


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11(reg_ref_clk == 0);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h3333333333333333, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda340000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:0, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:'d5, ireq_msg_seg:4'b0010, ireq_mbox:6'b000001, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11(reg_ref_clk == 0 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11(reg_ref_clk == 0); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11(reg_ref_clk == 1); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11(reg_ref_clk == 0); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F11(reg_ref_clk == 0);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11(reg_ref_clk ==1);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F11(reg_ref_clk == 1);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F11(reg_ref_clk == 1);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule

// The following rule provides the second 64 bit data
//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11_in1(reg_ref_clk == 1);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h2222222222222222, ireq_crf:False, ireq_prio:0, ireq_ftype:0, 						ireq_destid:0,ireq_addr:0, ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, 					ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11_in1(reg_ref_clk == 1 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11_in1(reg_ref_clk == 1);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11_in1(reg_ref_clk == 1); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11_in1(reg_ref_clk == 2); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11_in1(reg_ref_clk == 1); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11_in1(reg_ref_clk == 2); 
    $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F11_in1(reg_ref_clk == 1);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11_in1(reg_ref_clk ==2);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F11_in1(reg_ref_clk == 2 );
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F11_in1_in1(reg_ref_clk == 2);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule

// The following rule provides the third 64 bit data
//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11_in2(reg_ref_clk == 2);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:False, ireq_eof:True, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:0, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:0, ireq_ftype:0, 						ireq_destid:0,ireq_addr:0, ireq_hopcount:0, ireq_tid:0,ireq_ttype:0, ireq_byte_count:0, 					ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:0, ireq_msg_len:0, ireq_msg_seg:0, ireq_mbox:0, ireq_letter:0};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11_in2(reg_ref_clk == 2 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11_in2(reg_ref_clk == 2);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11_in2(reg_ref_clk == 2); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11_in2(reg_ref_clk == 3); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11_in2(reg_ref_clk == 2); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11_in2(reg_ref_clk == 3); 
    $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F11_in2(reg_ref_clk == 2);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11_in2(reg_ref_clk ==3);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule

// output from Generation Module 
rule rl_init_outputs_gen_F11_in2(reg_ref_clk == 3 );
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F11_in1_in2(reg_ref_clk == 3);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %h  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule
endmodule
endpackage
