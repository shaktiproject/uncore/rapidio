/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO_IOPkt_Concatenation Testbench
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of the RapidIO_IOPkt_Concatenation. 
-- 1. Generation of ftype 2 Packet.
-- 2. Generation of ftype 5 Packet.
-- 3. Generation of ftype 6 Packet.
-- 4. Generation of ftype 10 Packet.
-- 5. Generation of ftype 11 Packet.
-- 6. Generation of ftype 13 Packet.
-- 7. Generation of ftype 8 Packet.
-- 8. Generation of Initiator Request, Target response and Maintenance Response Packet.
-- 
--
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


package Tb_RapidIO_IOPkt_Concatenation;

import DefaultValue ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;

module mkTb_for_RapidIO_IOPkt_Concatenation(Empty);

Ifc_RapidIO_IOPktConcatenation in_pkt <- mkRapidIO_IOPktConcatenation;


Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);

Wire#(TargetRespIfcCntrl) wr_tar_control <- mkDWire (defaultValue);
Wire#(TargetRespIfcData) wr_tar_data <- mkDWire (defaultValue);
Wire#(TargetRespIfcMsg) wr_tar_msg <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet_con <- mkDWire (defaultValue);

Wire#(MaintenanceRespIfcCntrl) wr_main_control <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcData) wr_main_data <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet_con <- mkDWire (defaultValue);

Wire#(FType2_RequestClass) wr_ireq_FType2_RequestClass <- mkDWire (defaultValue);
Wire#(FType5_WriteClass) wr_ireq_FType5_WriteClass <- mkDWire (defaultValue);
Wire#(FType6_StreamWrClass) wr_ireq_FType6_StreamWrClass <- mkDWire (defaultValue);
Wire#(FType10_DOORBELLClass) wr_ireq_FType10_DOORBELLClass <- mkDWire (defaultValue);
Wire#(FType11_MESSAGEClass) wr_ireq_FType11_MESSAGEClass <- mkDWire (defaultValue);
Wire#(FType13_ResponseClass) wr_tar_FType13_ResponseClass <- mkDWire (defaultValue);
Wire#(FType8_MaintenanceClass) wr_main_FType8_RequestClass <- mkDWire (defaultValue);


Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 


// Clock Declaration 
Reg#(Bit#(6)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 12)
	$finish (0);
endrule


// -------------------Initiator request ----------------- Ftype2 ---------- Read Request Class ------------ //

rule rl_init_F2(reg_ref_clk == 0);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0010, ireq_destid:32'hda34568c, 			   			ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, ireq_byte_en:8'b11110000, 						ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F2_out(reg_ref_clk == 0); // Ftype packet from Concatenation Module is obtained as output
	wr_ireq_FType2_RequestClass <=in_pkt.outputs_Ftype2_IOReqClassPacket_ ();
endrule

rule rl_init__F2_compare(reg_ref_clk == 0); // To compare the input packet and generated packet 
	if(wr_ireq_packet == wr_ireq_packet_con)
	$display("\n  input and output are matched ");
	else
	$display("\n  input and output are not matched ");	
endrule

rule rl_disp_F2(reg_ref_clk == 0); // To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype2 = %b  \n Data Count = %b \n Ready from concatenation = %b", wr_ireq_packet, 		wr_ireq_packet_con,wr_ireq_FType2_RequestClass,wr_data_count,wr_ready_concatenation);     
endrule


// -------------------Initiator request ----------------- Ftype5 ---------- WriteClass ------------ //

rule rl_init_F5(reg_ref_clk == 1);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F5_out(reg_ref_clk == 1); // To obtain output packet from Concatenation Module 
	wr_ireq_FType5_WriteClass <=in_pkt.outputs_Ftype5_IOWrClassPacket_ ();
endrule

rule rl_disp_F5(reg_ref_clk == 1); // To display input packet, output packet and the ftype packet 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype5 = %b  \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType5_WriteClass,wr_data_count,wr_ready_concatenation);     
endrule


// -------------------Initiator request ----------------- Ftype6 -------- Stream Write Class --------------- //

rule rl_init_F6(reg_ref_clk == 2);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0110, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F6_out(reg_ref_clk == 2); // To obtain output packet from Concatenation Module 
	wr_ireq_FType6_StreamWrClass <=in_pkt.outputs_Ftype6_IOStreamWrClassPacket_ ();
endrule

rule rl_disp_F6(reg_ref_clk == 2); // To display input packet, output packet and the ftype packet 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype6 = %b  \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType6_StreamWrClass,wr_data_count,wr_ready_concatenation);     
endrule


// -------------------Initiator request ----------------- Ftype 10 ------ DOORBELL Class ---------------- //

rule rl_init_F10(reg_ref_clk == 3);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1010, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0,ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F10_out(reg_ref_clk == 3); // To obtain output packet from Concatenation Module 
	wr_ireq_FType10_DOORBELLClass <=in_pkt.outputs_Ftype10_MgDOORBELLClass_ ();
endrule

rule rl_disp_F10(reg_ref_clk == 3); // To display input packet, output packet and the ftype packet 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype10 = %b \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType10_DOORBELLClass,wr_data_count,wr_ready_concatenation);     
endrule


// -------------------Initiator request ----------------- Ftype 11 ------ MESSAGEClass ---------------- //

// For ireq_msg_len != 4'b0000

rule rl_init_F11(reg_ref_clk == 4);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F11_out(reg_ref_clk == 4); // To obtain output packet from Concatenation Module 
	wr_ireq_FType11_MESSAGEClass <=in_pkt.outputs_Ftype11_MESSAGEClass_ ();
endrule

rule rl_disp_F11(reg_ref_clk == 4); // To display input packet, output packet and the ftype packet 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype11 = %b  \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType11_MESSAGEClass,wr_data_count,wr_ready_concatenation);     
endrule

// For ireq_msg_len == 4'b0000

rule rl_init_F11_0_(reg_ref_clk == 5);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0000, ireq_msg_seg:4'b0010, ireq_mbox:6'b001100, ireq_letter:2'b10};
endrule

// Understand the rule from line number 229-245 and then proceed

rule rl_init_F11_3_(reg_ref_clk == 5); // To obtain output packet from Concatenation Module 
	wr_ireq_FType11_MESSAGEClass <=in_pkt.outputs_Ftype11_MESSAGEClass_ ();

endrule

rule rl_disp_F11_(reg_ref_clk == 5); // To display input packet, output packet and the ftype packet 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype11 = %b  \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType11_MESSAGEClass,wr_data_count,wr_ready_concatenation);     
endrule

// Common to both ftype 2, 5, 6, 10 and 11 

// Packet is formed by concatenating Control, Data and Message feilds 
rule rl_init_pkt(reg_ref_clk == 0 ||reg_ref_clk == 1 ||reg_ref_clk == 2 ||reg_ref_clk == 3 ||reg_ref_clk == 4 ||reg_ref_clk == 5);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The obtained packet from above rule is provied to concatenation module
rule rl_init_inputs(reg_ref_clk == 0 ||reg_ref_clk == 1 ||reg_ref_clk == 2 ||reg_ref_clk == 3 ||reg_ref_clk == 4 ||reg_ref_clk == 5);   
	in_pkt._inputs_InitReqIfcPkt (wr_ireq_packet); 
	in_pkt._inputs_RxReady_From_IOGeneration(True);
endrule

// packet from Concatenation Module is obtained as output
rule rl_init_outputs(reg_ref_clk == 0 ||reg_ref_clk == 1 ||reg_ref_clk == 2 ||reg_ref_clk == 3 ||reg_ref_clk == 4 ||reg_ref_clk == 5); 
	wr_ireq_packet_con<= in_pkt.outputs_InitReqIfcPkt_ ();
	wr_data_count <=in_pkt.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= in_pkt.outputs_RxRdy_From_Concat_ ();
endrule


// -------------------- Target response ---------------- ResponseClass ---------------------------------//

// Response class (ttype = 1) //

rule rl_tar_F13t1(reg_ref_clk == 6);  // The response signal from the Target processor 
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b10, tresp_data:64'h9999999999999999, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda34568c,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b0001, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Understand the rule from line number 270-297 and then proceed

// Response class (ttype = 0 and vld = false) 

rule rl_tar_F13t0_0(reg_ref_clk == 7);  // The response signal from the Target processor 
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:False, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b10, tresp_data:64'h8888888888888888, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	              	  tresp_dest_id:32'hda34568c,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b0000, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Common to both target response of type 0 and 1 

// Packet is formed by concatenating Control, Data and Message feilds 
rule rl_tar_F13_pkt(reg_ref_clk == 6 || reg_ref_clk == 7); 
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
endrule

// The obtained packet from above rule is provied to concatenation module
rule rl_tar_F13_inputs(reg_ref_clk == 6 || reg_ref_clk == 7); 
	in_pkt._inputs_TargetRespIfcPkt (wr_tar_packet); 
	in_pkt._inputs_RxReady_From_IOGeneration(True);
endrule

// packet from Concatenation Module is obtained as output
rule rl_tar_F13_outputs(reg_ref_clk == 6 || reg_ref_clk == 7); 
	wr_tar_packet_con<= in_pkt.outputs_TgtRespIfcPkt_ (); 
	wr_ready_concatenation <= in_pkt.outputs_RxRdy_From_Concat_ ();
	let abc=in_pkt.outputs_Ftype13_IORespPacket_ ();
	wr_tar_FType13_ResponseClass <=abc;
	if(abc.data matches tagged Valid .x)
		$display("The data is %h",x);
	else
		$display("Data is invalid");
	wr_data_count <=in_pkt.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet and the ftype packet 
rule rl_disp_F13(reg_ref_clk == 6 ||reg_ref_clk == 7); 
     $display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype13 = %b \n Data Count = %b \n Ready from concatenation = %b",wr_tar_packet,wr_tar_packet_con,wr_tar_FType13_ResponseClass,wr_data_count,wr_ready_concatenation);     
endrule


// ------------------ Maintenance Response ---------------- Ftype 8 --------------------- //

// Response Class - Read Response 

rule rl_main_F8t2(reg_ref_clk == 8);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b10, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda34568c,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0010, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Understand the rule from line number 319-346 and then proceed
// Response Class - Write Response 

rule rl_main_F8t3(reg_ref_clk == 9);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b10, mresp_data:64'h7777777777777777, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda34568c,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0011, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Common to both read and write response 

// Packet is formed by concatenating Control, Data and Message feilds 
rule rl_main_F8_pkt_resp(reg_ref_clk == 8 || reg_ref_clk == 9);  
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The obtained packet from above rule is provied to concatenation module
rule rl_main_F8_inputs_resp(reg_ref_clk == 8 || reg_ref_clk == 9); 
	in_pkt._inputs_MaintenanceIfcPkt (wr_main_packet); 
	in_pkt._inputs_RxReady_From_IOGeneration(True);
endrule

// packet from Concatenation Module is obtained as output
rule rl_main_F8_outputs_resp(reg_ref_clk == 8 || reg_ref_clk == 9); 
	wr_main_packet_con<= in_pkt.outputs_MaintainRespIfcPkt_ ();
	wr_ready_concatenation <= in_pkt.outputs_RxRdy_From_Concat_ ();
	let abc=in_pkt.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_FType8_RequestClass <=abc;
	if(abc.data matches tagged Valid .x)
		$display("The data is %h",x);
	else
		$display("Data is invalid");
	wr_data_count <=in_pkt.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet and the ftype packet 
rule rl_disp_F8_resp(reg_ref_clk == 8 || reg_ref_clk == 9); 
     	$display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype8 = %b \n Data Count = %b \n Ready from concatenation = %b",wr_main_packet, 			  wr_main_packet_con,wr_main_FType8_RequestClass,wr_data_count,wr_ready_concatenation);     
endrule

// Response Class - Read Request 

rule rl_main_F8t0(reg_ref_clk == 10);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0000, ireq_byte_count:'d4, 						ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule
// Understand the rule from line number 366-387 and then proceed
// Response Class - Write Request 

rule rl_main_F8t1(reg_ref_clk == 11);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0001, ireq_byte_count:'d4, 						ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Common to both read and write request 
// Packet is formed by concatenating Control, Data and Message feilds 
rule rl_main_F8_pkt_req(reg_ref_clk == 10 || reg_ref_clk == 11);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The obtained packet from above rule is provied to concatenation module
rule rl_main_F8_inputs_req(reg_ref_clk == 10 || reg_ref_clk == 11);  
	in_pkt._inputs_InitReqIfcPkt (wr_ireq_packet); 
	in_pkt._inputs_RxReady_From_IOGeneration(True);
endrule

// packet from Concatenation Module is obtained as output
rule rl_main_F8_outputs_req(reg_ref_clk == 10 || reg_ref_clk == 11); 
	wr_ireq_packet_con<= in_pkt.outputs_InitReqIfcPkt_ ();
	wr_ready_concatenation <= in_pkt.outputs_RxRdy_From_Concat_ ();
	let abc=in_pkt.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_FType8_RequestClass <=abc;
	if(abc.data matches tagged Valid .x)
		$display("The data is %h",x);
	else
		$display("Data is invalid");
	wr_data_count <=in_pkt.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet and the ftype packet
rule rl_disp_F8_req(reg_ref_clk == 10 || reg_ref_clk == 11);  
     	$display(" \n input packet =  %b  \n\n output packet = %b  \n\n Ftype8 = %b \n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet, 			  wr_ireq_packet_con,wr_main_FType8_RequestClass,wr_data_count,wr_ready_concatenation);     
endrule


// Initiator request, target response and maintenance response is given at a time 

// Initiator request ftype 5 (Nwrite) 

rule rl_init(reg_ref_clk == 12);  // To generate Control, Data, Msg Signals for Initiator Request 
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b10, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, 						ireq_destid:32'hda34568c,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Target Response ftype = 13 ttype = 1 

rule rl_tar(reg_ref_clk == 12);  // The response signal from the Target processor 
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b10, tresp_data:64'h9999999999999999, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda34568c,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b0001, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Maintenance Response ftype = 8, ttype = 3 

rule rl_main(reg_ref_clk == 12);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b10, mresp_data:64'h7777777777777777, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda34568c,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0011, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Packet is formed by concatenating Control, Data and Message by the above 3 rules
rule rl_pkt(reg_ref_clk == 12);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The obtained packet from above rule is provied to concatenation module
rule rl_inputs(reg_ref_clk == 12); 
	in_pkt._inputs_InitReqIfcPkt (wr_ireq_packet);
	in_pkt._inputs_TargetRespIfcPkt (wr_tar_packet); 
	in_pkt._inputs_MaintenanceIfcPkt (wr_main_packet); 
	in_pkt._inputs_RxReady_From_IOGeneration(True);
endrule

// packet from Concatenation Module is obtained as output
rule rl_outputs(reg_ref_clk == 12); 
	
	wr_ireq_FType2_RequestClass <=in_pkt.outputs_Ftype2_IOReqClassPacket_ ();	
	wr_ireq_FType5_WriteClass <=in_pkt.outputs_Ftype5_IOWrClassPacket_ ();
	wr_ireq_FType6_StreamWrClass <=in_pkt.outputs_Ftype6_IOStreamWrClassPacket_ ();
	wr_ireq_FType10_DOORBELLClass <=in_pkt.outputs_Ftype10_MgDOORBELLClass_ ();
	wr_ireq_FType11_MESSAGEClass <=in_pkt.outputs_Ftype11_MESSAGEClass_ ();
	wr_tar_FType13_ResponseClass <=in_pkt.outputs_Ftype13_IORespPacket_ ();
	wr_main_FType8_RequestClass <=in_pkt.outputs_Ftype8_IOMaintenancePacket_ ();

	wr_ireq_packet_con<= in_pkt.outputs_InitReqIfcPkt_ ();
	wr_main_packet_con<= in_pkt.outputs_MaintainRespIfcPkt_ ();
	wr_tar_packet_con<= in_pkt.outputs_TgtRespIfcPkt_ ();	

	wr_data_count <=in_pkt.outputs_InitReqDataCount_ ();

	wr_ready_concatenation <= in_pkt.outputs_RxRdy_From_Concat_ ();
	
endrule

// To display input packets, output packets and the ftype packets 
rule rl_disp(reg_ref_clk == 12); 
     	$display(" \n----------- input packets-------- \n\n Initiator request signals =  %b  \n\n Target reponse signals = %b \n\n Maintenance response signals = %b 			\n\n----------------------- output packets --------------\n\n Initiator request = %b  \n\n Target reponse signals = %b \n\n Maintenance response signals = %b \n\n Ftype2 = %b \n Ftype5 = %b \n Ftype6 = %b \n Ftype10 = %b \n Ftype11 = %b \n Ftype13 = %b \n Ftype8 = %b \n\n Data Count = %b \n Ready from concatenation = %b",wr_ireq_packet, wr_tar_packet,wr_main_packet,wr_ireq_packet_con,wr_tar_packet_con,wr_main_packet_con, wr_ireq_FType2_RequestClass, wr_ireq_FType5_WriteClass,wr_ireq_FType6_StreamWrClass,wr_ireq_FType10_DOORBELLClass,wr_ireq_FType11_MESSAGEClass,wr_tar_FType13_ResponseClass, wr_main_FType8_RequestClass,wr_data_count,wr_ready_concatenation);     
endrule


endmodule
endpackage
