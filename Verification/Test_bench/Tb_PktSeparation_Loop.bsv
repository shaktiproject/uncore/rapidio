/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- Test Bench For RapidIO_IOPktSeparation Module
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This testbench verifies the functionality of both the RapidIO_IOPkt_concatenation module, RapidIO_IOPkt_Generation module and RapidIO_PktSeparation Module.
-- 1. Initiator Request, target response and maintenance packets are provided as input to concatenation module.
-- 2. Output SOF, EOF, Vld, Data (Header or Data), TxRem and Crf are obtained fron generation module.
3. The output from generation module is given directly to Packet Separation module.
--
--
-- Author(s):
-- Ajoy C A (ajoyca141@gmail.com)
-- M.Gopinathan (gopinathan18@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2014, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_PktSeparation_Loop;

import DefaultValue ::*;
import RapidIO_IOPkt_Concatenation ::*;
import RapidIO_IOPkt_Generation ::*;
import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import RapidIO_InitiatorReqIFC ::*;
import RapidIO_TargetRespIFC ::*;
import RapidIO_MaintenanceRespIFC ::*;
import RapidIO_InitEncoder_WdPtr_Size ::*;
import RapidIO_InComingPkt_Separation ::*;

module mkTb_for_PktSeparation_Loop(Empty);

Ifc_RapidIO_IOPktConcatenation ifc_con <- mkRapidIO_IOPktConcatenation;
Ifc_RapidIO_IOPkt_Generation ifc_gen <- mkRapidIO_IOPkt_Generation;
Ifc_RapidIO_InComingPkt_Separation ifc_sep <- mkRapidIO_InComingPkt_Separation;

// For Concatenation Module

Wire#(InitReqIfcCntrl) wr_ireq_control <- mkDWire (defaultValue);
Wire#(InitReqIfcData) wr_ireq_data <- mkDWire (defaultValue);
Wire#(InitReqIfcMsg) wr_ireq_msg <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet <- mkDWire (defaultValue);
Wire#(InitiatorReqIfcPkt) wr_ireq_packet_con <- mkDWire (defaultValue);

Wire#(TargetRespIfcCntrl) wr_tar_control <- mkDWire (defaultValue);
Wire#(TargetRespIfcData) wr_tar_data <- mkDWire (defaultValue);
Wire#(TargetRespIfcMsg) wr_tar_msg <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet <- mkDWire (defaultValue);
Wire#(TargetRespIfcPkt) wr_tar_packet_con <- mkDWire (defaultValue);

Wire#(MaintenanceRespIfcCntrl) wr_main_control <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcData) wr_main_data <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet <- mkDWire (defaultValue);
Wire#(MaintenanceRespIfcPkt) wr_main_packet_con <- mkDWire (defaultValue);

Wire#(FType2_RequestClass) wr_ireq_FType2_RequestClass <- mkDWire (defaultValue);
Wire#(FType5_WriteClass) wr_ireq_FType5_WriteClass <- mkDWire (defaultValue);
Wire#(FType6_StreamWrClass) wr_ireq_FType6_StreamWrClass <- mkDWire (defaultValue);
Wire#(FType10_DOORBELLClass) wr_ireq_FType10_DOORBELLClass <- mkDWire (defaultValue);
Wire#(FType11_MESSAGEClass) wr_ireq_FType11_MESSAGEClass <- mkDWire (defaultValue);
Wire#(FType13_ResponseClass) wr_tar_FType13_ResponseClass <- mkDWire (defaultValue);
Wire#(FType8_MaintenanceClass) wr_main_FType8_MaintenanceClass <- mkDWire (defaultValue);


Wire#(InitReqDataInput) wr_data_count <- mkDWire (defaultValue); 
Wire#(Bool) wr_ready_concatenation <- mkDWire (False); 

// For Generation Module

Wire#(Bool) wr_gen_SOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_EOF_n <- mkDWire (False);
Wire#(Bool) wr_gen_DSC_n <- mkDWire (False);
Wire#(Bool) wr_gen_VLD_n <- mkDWire (False);
Wire#(DataPkt) wr_gen_Data <- mkDWire (defaultValue); 
Wire#(Bit#(4)) wr_gen_TxRem <- mkDWire (0);
Wire#(Bool) wr_gen_Crf <- mkDWire (False); 
Wire#(Bool) wr_ready_from_dest <- mkDWire (False); 

// For Separation Module
Wire#(DataPkt) wr_HeaderPkt <- mkDWire (0); 
Wire#(DataPkt) wr_DataPkt <- mkDWire (0); 
Wire#(Bit#(4)) wr_PktCount <- mkDWire (0);
Wire#(Bool) wr_LastPkt <-mkDWire (False);
Wire#(Bit#(4)) wr_Pkt_Count <- mkDWire (0);



// Clock Declaration 
Reg#(Bit#(5)) reg_ref_clk <- mkReg (0);	
  
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n [  ------------------------------- CLOCK == %d -----------------------------------------  ]", reg_ref_clk);
	if (reg_ref_clk == 30)
	$finish (0);
endrule


// -------------------Initiator request ----------------- Ftype2 ---------- Read Request Class ------------ //

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F2(reg_ref_clk == 0);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:0, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0010, ireq_destid:32'hda34568c, 			   			ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:0, ireq_byte_en:0, 						ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0000, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F2(reg_ref_clk == 0 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F2(reg_ref_clk == 0);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F2(reg_ref_clk == 0); 
	wr_ireq_FType2_RequestClass <=ifc_con.outputs_Ftype2_IOReqClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F2(reg_ref_clk == 1); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F2(reg_ref_clk == 0); 
     $display(" \n # -----Initiator request ----------------- Ftype2 ---------- Read Request Class ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n\n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype2      = %b                                  			 \n Ready from concatenation to initiator request = %b", wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType2_RequestClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F2(reg_ref_clk == 1); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F2(reg_ref_clk == 0);   
	ifc_gen._inputs_Ftype2IOReqClass (wr_ireq_FType2_RequestClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F2(reg_ref_clk ==1);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F2(reg_ref_clk == 1); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F2(reg_ref_clk == 1); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule


// Input to Separation Module
rule rl_input_sep_F2(reg_ref_clk == 1);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F2(reg_ref_clk == 2);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F2(reg_ref_clk == 2); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// -------------------Initiator request ----------------- Ftype5 ---------- WriteClass ------------ //


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F5(reg_ref_clk == 2);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0101, 						ireq_destid:32'hdabc0000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d4, 						ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F5(reg_ref_clk == 2 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F5(reg_ref_clk == 2);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F5(reg_ref_clk == 2); 
	wr_ireq_FType5_WriteClass <=ifc_con.outputs_Ftype5_IOWrClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F5(reg_ref_clk == 3); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F5(reg_ref_clk == 2); 
     $display(" \n # -----Initiator request ----------------- Ftype5 ---------- WriteClass  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype5      = %b 						    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet,wr_ireq_packet_con,wr_ireq_FType5_WriteClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F5(reg_ref_clk == 3); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F5(reg_ref_clk == 2);   
	ifc_gen._inputs_Ftype5IOWrClass (wr_ireq_FType5_WriteClass );
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F5(reg_ref_clk ==3);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F5(reg_ref_clk == 3 || reg_ref_clk == 4); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F5(reg_ref_clk == 3 || reg_ref_clk == 4 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F5(reg_ref_clk == 3 || reg_ref_clk == 4 );
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F5(reg_ref_clk == 4 ||reg_ref_clk == 5);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F5(reg_ref_clk == 4 ||reg_ref_clk == 5); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packet    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule


// -------------------Initiator request ----------------- Ftype6 ---------- Streaming Write Class ------------ //


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F6(reg_ref_clk == 5);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h9999999999999999, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b0110, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F6(reg_ref_clk == 5 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F6(reg_ref_clk == 5);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F6(reg_ref_clk == 5); 
	wr_ireq_FType6_StreamWrClass <=ifc_con.outputs_Ftype6_IOStreamWrClassPacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F6(reg_ref_clk == 6); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F6(reg_ref_clk == 5); 
     $display(" \n # -----Initiator request -------------- Ftype6 ---------- Streaming Write Class ------- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype6      = %b                	                      			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType6_StreamWrClass, 			  			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F6(reg_ref_clk == 6); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F6(reg_ref_clk == 5);   
	ifc_gen._inputs_Ftype6IOStreamClass (wr_ireq_FType6_StreamWrClass );
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F6(reg_ref_clk ==6);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F6(reg_ref_clk == 6 || reg_ref_clk == 7); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F6(reg_ref_clk == 6 || reg_ref_clk == 7 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 	                                    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem, wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F6(reg_ref_clk == 6 || reg_ref_clk == 7);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F6(reg_ref_clk == 7 || reg_ref_clk == 8);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F6(reg_ref_clk == 7 || reg_ref_clk == 8); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule


// -------------------Initiator request ----------------- Ftype 10 ---------- DOORBELLClass ------------ //


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F10(reg_ref_clk == 8);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:True, ireq_vld:True, ireq_dsc:True};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1010, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0,ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:0, 						ireq_byte_en:0, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F10(reg_ref_clk == 8 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F10(reg_ref_clk == 8);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F10(reg_ref_clk == 8); 
	wr_ireq_FType10_DOORBELLClass <= ifc_con.outputs_Ftype10_MgDOORBELLClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F10(reg_ref_clk == 9); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F10(reg_ref_clk == 8); 
     $display(" \n # -----Initiator request ----------------- Ftype 10 ---------- DOORBELLClass ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype10     = %b  		 				    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType10_DOORBELLClass,	 			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F10(reg_ref_clk == 9); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule


// Input to generation module
rule rl_init_inputs_gen_F10(reg_ref_clk == 8);   
	ifc_gen._inputs_Ftype10MgDOORBELLClass (wr_ireq_FType10_DOORBELLClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F10(reg_ref_clk ==9);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F10(reg_ref_clk == 9 ); 
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F10(reg_ref_clk == 9 ); 
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b                                         			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 			 wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F10(reg_ref_clk == 9);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F10(reg_ref_clk == 10);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F10(reg_ref_clk == 10); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule


// -------------------Initiator request ----------------- Ftype 11 ---------- MESSAGEClass ------------ //


//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F11(reg_ref_clk == 10);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b01, ireq_data:64'h2222222222222222, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1011, 						ireq_destid:32'hda340000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0100, ireq_byte_count:'d256, 						ireq_byte_en:8'hff, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0000, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F11(reg_ref_clk == 10 );  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F11(reg_ref_clk == 10);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F11(reg_ref_clk == 10); 
	wr_ireq_FType11_MESSAGEClass <= ifc_con.outputs_Ftype11_MESSAGEClass_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F11(reg_ref_clk == 11); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F11(reg_ref_clk == 10); 
     $display(" \n # -----Initiator request ----------------- Ftype11 ---------- MESSAGEClass ----- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype11     = %b                                                 			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_ireq_FType11_MESSAGEClass,                            			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F11(reg_ref_clk == 11); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F11(reg_ref_clk == 10);   
	ifc_gen._inputs_Ftype11MESSAGEClass (wr_ireq_FType11_MESSAGEClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F11(reg_ref_clk ==11);   
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F11(reg_ref_clk == 11 || reg_ref_clk == 12 );
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F11(reg_ref_clk == 11 || reg_ref_clk == 12);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n, wr_gen_EOF_n, wr_gen_VLD_n, wr_gen_DSC_n, wr_gen_Data, wr_gen_TxRem, wr_gen_Crf, 		wr_ready_from_dest);     
endrule


// Input to Separation Module
rule rl_input_sep_F11(reg_ref_clk == 11 || reg_ref_clk == 12);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F11(reg_ref_clk == 12 || reg_ref_clk == 13);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F11(reg_ref_clk == 12 || reg_ref_clk == 13); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// -------------------- Target response ---------------- ResponseClass ---------------------------------//

// Response class (ttype = 1) with Payload

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F13_p(reg_ref_clk == 13);  
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b00, tresp_data:64'h3333333333333333, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda000000,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b1000, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F13_p(reg_ref_clk == 13);  
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F13_p(reg_ref_clk == 13);   
	ifc_con._inputs_TargetRespIfcPkt (wr_tar_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F13_p(reg_ref_clk == 13);
	wr_tar_FType13_ResponseClass <= ifc_con.outputs_Ftype13_IORespPacket_ ();
	wr_tar_packet_con<= ifc_con.outputs_TgtRespIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F13_p(reg_ref_clk == 14);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F13_p(reg_ref_clk == 13);
     $display(" \n # ----- Target response ----------------- Ftype13 ---------- Response class with Payload  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype13     = %b              					    			\n Ready from concatenation to initiator request = %b", wr_tar_packet, wr_tar_packet_con, wr_tar_FType13_ResponseClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F13_p(reg_ref_clk == 14); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F13_p(reg_ref_clk == 13);  
	ifc_gen._inputs_Ftype13IORespClass (wr_tar_FType13_ResponseClass);
	ifc_gen._inputs_TgtRespIfcPkt (wr_tar_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F13_p(reg_ref_clk == 14);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F13_p(reg_ref_clk == 14);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F13_p(reg_ref_clk == 14);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F13_p(reg_ref_clk == 14);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F13_p(reg_ref_clk == 15);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F13_p(reg_ref_clk == 15); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// Response class (ttype = 0) without Payload

//  Control, Data, Msg Signals are provied to create reference packet 
rule rl_init_F13(reg_ref_clk == 15);  
	wr_tar_control <= TargetRespIfcCntrl {tresp_sof:True, tresp_eof:False, tresp_vld:True, tresp_dsc:False};	
	wr_tar_data <= TargetRespIfcData {tresp_tt:2'b00, tresp_data:64'h3333333333333333, tresp_crf:False, tresp_prio:2'b01, tresp_ftype:4'b1101, 			   	                  tresp_dest_id:32'hda000000,tresp_status:4'b0000,tresp_tid:8'hbf,tresp_ttype:4'b0000, tresp_no_data:False};
	wr_tar_msg <= TargetRespIfcMsg {tresp_msg_seg:4'b0010, tresp_mbox:2'b10, tresp_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F13(reg_ref_clk == 15);  
	wr_tar_packet<= TargetRespIfcPkt {trespcntrl:wr_tar_control, trespdata:wr_tar_data, trespmsg:wr_tar_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F13(reg_ref_clk == 15);   
	ifc_con._inputs_TargetRespIfcPkt (wr_tar_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F13(reg_ref_clk == 15);
	wr_tar_FType13_ResponseClass <= ifc_con.outputs_Ftype13_IORespPacket_ ();
	wr_tar_packet_con<= ifc_con.outputs_TgtRespIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F13(reg_ref_clk == 16);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F13(reg_ref_clk == 15);
     $display(" \n # ----- Target response ---------------- Ftype13 ----------Response class without Payload  ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype13     = %b						    			\n Ready from concatenation to initiator request = %b", wr_tar_packet, wr_tar_packet_con, wr_tar_FType13_ResponseClass, wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F13(reg_ref_clk == 16); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F13(reg_ref_clk == 15);  
	ifc_gen._inputs_Ftype13IORespClass (wr_tar_FType13_ResponseClass);
	ifc_gen._inputs_TgtRespIfcPkt (wr_tar_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F13(reg_ref_clk == 16);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F13(reg_ref_clk == 16);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F13(reg_ref_clk == 16);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    		 	\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F13(reg_ref_clk == 16);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F13(reg_ref_clk == 17);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F13(reg_ref_clk == 17); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// ------------------ Maintenance Response ---------------- Ftype 8 --------------------- //

// Response Class - Read Response

//  Control and Data Signals are provied to create reference packet 
rule rl_init_F8_Rd_resp(reg_ref_clk == 17);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b01, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda340000,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0010, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Rd_resp(reg_ref_clk == 17);  
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Rd_resp(reg_ref_clk == 17);   
	ifc_con._inputs_MaintenanceIfcPkt (wr_main_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Rd_resp(reg_ref_clk == 17);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_packet_con<= ifc_con.outputs_MaintainRespIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Rd_resp(reg_ref_clk == 18);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Rd_resp(reg_ref_clk == 17);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ---------- Read Response ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b 						    			\n Ready from concatenation to initiator request = %b", wr_main_packet, wr_main_packet_con, wr_main_FType8_MaintenanceClass ,     			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Rd_resp(reg_ref_clk == 18); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Rd_resp(reg_ref_clk == 17);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_MaintenanceRespIfcPkt (wr_main_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Rd_resp(reg_ref_clk == 18);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Rd_resp(reg_ref_clk == 18 || reg_ref_clk == 19);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Rd_resp(reg_ref_clk == 18 || reg_ref_clk == 19);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b \n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf,wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F8_Rd_resp(reg_ref_clk == 18 || reg_ref_clk == 19);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F8_Rd_resp(reg_ref_clk == 19 || reg_ref_clk == 20);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F8_Rd_resp(reg_ref_clk == 19 || reg_ref_clk == 20); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// Response Class - Write Response

//  Control and Data Signals are provied to create reference packet 
rule rl_init_F8_Wr_resp(reg_ref_clk == 20);  
	wr_main_control <= MaintenanceRespIfcCntrl {mresp_sof:True, mresp_eof:False, mresp_vld:True};	
	wr_main_data <= MaintenanceRespIfcData {mresp_tt:2'b01, mresp_data:64'h9999999999999999, mresp_crf:False, mresp_prio:2'b01, mresp_ftype:4'b1000, 			   	              		mresp_dest_id:32'hda340000,mresp_status:4'b0000,mresp_tid:8'hbf,mresp_ttype:4'b0011, 							mresp_hop_count:8'hbb,mresp_local:True};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Wr_resp(reg_ref_clk == 20);  
	wr_main_packet<= MaintenanceRespIfcPkt {mrespcntrl:wr_main_control, mrespdata:wr_main_data};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Wr_resp(reg_ref_clk == 20);   
	ifc_con._inputs_MaintenanceIfcPkt (wr_main_packet); 
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Wr_resp(reg_ref_clk == 20);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_main_packet_con<= ifc_con.outputs_MaintainRespIfcPkt_ (); 
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Wr_resp(reg_ref_clk == 21);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule

// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Wr_resp(reg_ref_clk == 20);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ----------Write Response --- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b 	       					    			\n Ready from concatenation to initiator request = %b", wr_main_packet, wr_main_packet_con, wr_main_FType8_MaintenanceClass ,    			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Wr_resp(reg_ref_clk == 21); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Wr_resp(reg_ref_clk == 20);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_MaintenanceRespIfcPkt (wr_main_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Wr_resp(reg_ref_clk == 21);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Wr_resp(reg_ref_clk == 21);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Wr_resp(reg_ref_clk == 21);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b \n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf,wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F8_Wr_resp(reg_ref_clk == 21);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F8_Wr_resp(reg_ref_clk == 22);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F8_Wr_resp(reg_ref_clk == 22); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// Response Class - Read Request

//  Control, Data and Msg Signals are provied to create reference packet 
rule rl_init_F8_Rd_req(reg_ref_clk == 22);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0000, ireq_byte_count:'d4, 						ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Rd_req(reg_ref_clk == 22);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Rd_req(reg_ref_clk == 22);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet);
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Rd_req(reg_ref_clk == 22);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Rd_req(reg_ref_clk == 23);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Rd_req(reg_ref_clk == 22);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ---------- Read Request ---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b     					            			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_main_FType8_MaintenanceClass ,    			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Rd_req(reg_ref_clk == 23); 
     $display("\n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Rd_req(reg_ref_clk == 22);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Rd_req(reg_ref_clk == 23);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Rd_req(reg_ref_clk == 23);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Rd_req(reg_ref_clk == 23);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b \n\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf,wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F8_Rd_req(reg_ref_clk == 23);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F8_Rd_req(reg_ref_clk == 24);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F8_Rd_req(reg_ref_clk == 24); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

// Response Class - Write Request

//  Control, Data and Msg Signals are provied to create reference packet 
rule rl_init_F8_Wr_req(reg_ref_clk == 24);  
	wr_ireq_control <= InitReqIfcCntrl {ireq_sof:True, ireq_eof:False, ireq_vld:True, ireq_dsc:False};	
	wr_ireq_data <= InitReqIfcData {ireq_tt:2'b00, ireq_data:64'h1111111111111111, ireq_crf:False, ireq_prio:2'b01, ireq_ftype:4'b1000, 						ireq_destid:32'hda000000,ireq_addr:50'h000000008, ireq_hopcount:0, ireq_tid:8'hbf,ireq_ttype:4'b0001, ireq_byte_count:'d4, 						ireq_byte_en:8'b11110000, ireq_local:False};
	wr_ireq_msg <= InitReqIfcMsg {ireq_db_info:16'b0000111100001111, ireq_msg_len:4'b0110, ireq_msg_seg:4'b0010, ireq_mbox:6'b001000, ireq_letter:2'b10};
endrule

// Reference Packet is formed by concatenating above feilds 
rule rl_init_pkt_F8_Wr_req(reg_ref_clk == 24);  
	wr_ireq_packet<= InitiatorReqIfcPkt {ireqcntrl:wr_ireq_control, ireqdata:wr_ireq_data, ireqmsg:wr_ireq_msg};
endrule

// The Reference packet is provied to concatenation module
rule rl_init_inputs_con_F8_Wr_req(reg_ref_clk == 24);   
	ifc_con._inputs_InitReqIfcPkt (wr_ireq_packet);
	ifc_con._inputs_RxReady_From_IOGeneration(False);
endrule		

// To obtain output packet from Concatenation Module 
rule rl_init_outputs_con_F8_Wr_req(reg_ref_clk == 24);
	wr_main_FType8_MaintenanceClass <= ifc_con.outputs_Ftype8_IOMaintenancePacket_ ();
	wr_ireq_packet_con<= ifc_con.outputs_InitReqIfcPkt_ (); 
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
	wr_ready_concatenation <= ifc_con.outputs_RxRdy_From_Concat_ ();
endrule

rule rl_init_outputs_con_datacount_F8_Wr_req(reg_ref_clk == 25);
	wr_data_count <=ifc_con.outputs_InitReqDataCount_ ();
endrule
// To display input packet, output packet ,ftype packet, Data count, Ready Signal From Concatenation 
rule rl_disp_con_F8_Wr_req(reg_ref_clk == 24);
     $display(" \n # -----Maintenance Response  ----------------- Ftype8 ----------Write Request---- #");
     $display(" \n # -------Concatenation Module ------------- #");
     $display(" \n Reference input packet   =  %b  \n\n packet provided to generation = %b  \n\n Ftype 8    = %b  						    			\n Ready from concatenation to initiator request = %b", wr_ireq_packet, wr_ireq_packet_con, wr_main_FType8_MaintenanceClass,         			wr_ready_concatenation);     
endrule

rule rl_disp_con_datacount_F8_Wr_req(reg_ref_clk == 25); 
     $display("   \n Data Count  = %b ",wr_data_count);     
endrule

// Input to generation module
rule rl_init_inputs_gen_F8_Wr_req(reg_ref_clk == 24);  
	ifc_gen._inputs_Ftype8IOMaintenanceClass (wr_main_FType8_MaintenanceClass);
	ifc_gen._inputs_InitReqIfcPkt (wr_ireq_packet_con);
	ifc_gen.pkgen_rdy_n (wr_ready_concatenation);
endrule

rule rl_init_inputs_gen_datacount_F8_Wr_req(reg_ref_clk == 25);  
	ifc_gen._inputs_InitReqDataCount (wr_data_count); 
endrule


// output from Generation Module 
rule rl_init_outputs_gen_F8_Wr_req(reg_ref_clk == 25 || reg_ref_clk == 26);
	wr_gen_SOF_n<= ifc_gen.pkgen_sof_n_ (); 
	wr_gen_EOF_n<= ifc_gen.pkgen_eof_n_ ();
	wr_gen_VLD_n<= ifc_gen.pkgen_vld_n_ ();
	wr_gen_DSC_n<= ifc_gen.pkgen_dsc_n_ ();	
	wr_gen_Data<= ifc_gen.pkgen_data_ ();
	wr_gen_TxRem <= ifc_gen.pkgen_tx_rem_ ();
	wr_gen_Crf<= ifc_gen.pkgen_crf_ ();
	wr_ready_from_dest<= ifc_gen.outputs_RxRdy_From_Dest_(); 
endrule

// To display output  comming from Generation 
rule rl_disp_gen_F8_Wr_req(reg_ref_clk == 25 || reg_ref_clk == 26);
     $display(" \n # -------IOPkt Generation Module ------------- #");
     $display(" \n Sof    = %b  \n Eof    = %b \n Vld    = %b  \n Dsc    = %b \n Data   = %b  \n TxRem  = %b \n Crf    = %b 					    			\n Ready from generation to concatenation = %b", wr_gen_SOF_n,wr_gen_EOF_n,wr_gen_VLD_n,wr_gen_DSC_n,wr_gen_Data,wr_gen_TxRem,wr_gen_Crf, 			wr_ready_from_dest);     
endrule

// Input to Separation Module
rule rl_input_sep_F8_Wr_req(reg_ref_clk == 25 || reg_ref_clk == 26);
	ifc_sep._inputs_DataPkt (wr_gen_Data);
	ifc_sep._inputs_SOF (!wr_gen_SOF_n);
	ifc_sep._inputs_EOF (!wr_gen_EOF_n);
	ifc_sep._inputs_VLD (!wr_gen_VLD_n);
endrule

rule rl_output_sep_F8_Wr_req(reg_ref_clk == 26 || reg_ref_clk == 27);
	wr_HeaderPkt <=	ifc_sep.outputs_HeaderPkt_ ();
	wr_DataPkt <=	ifc_sep.outputs_DataPkt_ ();
	wr_PktCount <=	ifc_sep.outputs_PktCount_ ();
	wr_LastPkt <=	ifc_sep.outputs_LastPkt_ ();
	wr_Pkt_Count <=	ifc_sep.outputs_MaxPktCount_ ();
endrule

rule rl_disp_sep_F8_Wr_req(reg_ref_clk == 26 || reg_ref_clk == 27); 
     $display(" \n # -------IOPkt Separation Module ------------- #");
     $display(" \n Header packet    = %b  \n Data Packrt    = %b \n Packet Count    = %b  \n Last Packet    = %b \n Max Packet Count   = %b",wr_HeaderPkt, 			wr_DataPkt,wr_PktCount,wr_LastPkt,wr_Pkt_Count);     
endrule

endmodule
endpackage
