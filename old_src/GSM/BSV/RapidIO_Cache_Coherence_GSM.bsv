// TODO transaction recognition 
// TODO handling multiple response packets for single request
// TODO Generate TransactionID here. This will be used to match req-resp pair.

/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Top Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- Working Of Cache Coherence Module 

--This module accepts signals from Ifc_InitiatorReqSignals.
--It decodes the fType and tType of transaction.
--Based on Valus of fType anfd tType it creates a coherence Packet (Packet UnderStood by Coherence Protocol)
--
--
--
--   
-- 
--
-- Author(s):
-- Anshu Kumar (akgeni@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/







package RapidIO_Cache_Coherence_GSM;

import RapidIO_DTypes::*;
import RapidIO_Cache_Coherence_Types::*;
import RapidIO_Cache_Coherence_Types::*;
import RapidIO_Cache_Coherence_Encode_packet::*;
import DefaultValue::*;
import RapidIO_InitiatorReqIFC::*;
import RapidIO_TargetReqIFC::*;
import RapidIO_InitiatorRespIFC::*;
import RapidIO_TargetRespIFC::*;
import RapidIO_Cache_Coherence_GSM_Interface ::*;
//import RapidIO_GSM_Arbitarion ::*;

//import RapidIO_Cache_Coherence_Home_Node::*;
import Vector::*;

interface Ifc_RapidIO;

method Action _inputs_Packet_To_RapidIO_From_Agent_Request(Packet pkt);
method Action _inputs_From_Processor_Agent_To_GSM_At_Remote(Packet pkt);
method Action _inputs_Packet_RapidIO_Response(Packet pkt);
method Packet _outputs_Packet_RapidIO_Local_Resp();  // Response received at local.
method Packet _outputs_Packet_RapidIO_Remote_Req();  // Request on Remote end.
method Bool   _outputs_ready_To_Receive_RapidIO();


interface Ifc_GSM _ifc_gsm;

/*
///   InitReq Signals as Output. Connecting to Wrapper.  ////////////
method Bool _ireq_sof_n();
method Bool _ireq_eof_n();
method Bool _ireq_vld_n();
method Bool _ireq_dsc_n ();
method Action ireq_rdy_n_ (Bool value);

method TT _ireq_tt();
method Data _ireq_data ();
method Bool _ireq_crf ();
method Prio _ireq_prio ();
method Type _ireq_ftype();
method DestId _ireq_dest_id ();
method Addr _ireq_addr(); 
method Bit#(8) _ireq_hopcount ( );
method TranId _ireq_tid (); 
method Type _ireq_ttype ( );
method ByteCount _ireq_byte_count ();
method ByteEn _ireq_byte_en_n ();
method Bool _ireq_local ();

method DoorBell  _ireq_db_info ();
method MsgLen _ireq_msg_len ();
method MsgSeg _ireq_msg_seg ();
method Bit#(6) _ireq_mbox ( );

method Bit#(2) _ireq_letter ();
	
	
	
/// Target Response Signals as Output.  Connecting to Wrapper ////////////////////////////////

 method Bool outputs_tresp_sof_n_ ();
	
 method Bool outputs_tresp_eof_n_ (); 
	
 method Bool outputs_tresp_vld_n_ ();
	
 method Bool outputs_tresp_dsc_n_ ();
	
 method Action _inputs_tresp_rdy_n_ (Bool value);

 method TT outputs_tresp_tt_ (); 
	
 method Data outputs_tresp_data_ ();
	
 method Bool outputs_tresp_crf_ ();
	
 method Prio outputs_tresp_prio_ ( );
	
 method Type outputs_tresp_ftype_ ();
	
 method DestId outputs_tresp_dest_id_ ();
	
 method Status outputs_tresp_status_ ();
	
 method TranId outputs_tresp_tid_ ();
	
 method Type outputs_tresp_ttype_ ();
	
 method Bool outputs_tresp_no_data_ ();

 method MsgSeg outputs_tresp_msg_seg_ ();
	
 method Bit#(2) outputs_tresp_mbox_ ( );
	
 method Mletter  outputs_tresp_letter_ ();
	
 
 
 
 /////////////////    Treq Methods /////
 
 method Action treq_sof_n_ (Bool value);
	
 method Action treq_eof_n_ (Bool value);
	
 method Action treq_vld_n_ (Bool value);
	
 method Bool _treq_rdy_n ();
	
 method Action treq_tt_ (TT value); 
	 
 method Action treq_data_ (Data value);
	
 method Action treq_crf_ (Bool value);
	
 method Action treq_prio_ (Prio value);
	
 method Action treq_ftype_ (Type value);
	
 method Action treq_dest_id_ (DestId value);
	
 method Action treq_source_id_ (SourceId value);
	
 method Action treq_tid_ (TranId value);
	
 method Action treq_ttype_ (Type value);
	
 method Action treq_addr_ (Addr value);
	
 method Action treq_byte_count_ (ByteCount value);
	
 method Action treq_byte_en_n_ (ByteEn value);
	
	
	
//////////////////    Iresp Methods  ///////////////



//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value); 	
 method Action iresp_eof_n_ (Bool value); 
 method Action iresp_vld_n_ (Bool value);	
 method Bool _iresp_rdy_n (); 	

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value);  	
 method Action iresp_data_ (Data value); 	
 method Action iresp_crf_ (Bool value); 
 method Action iresp_prio_ (Prio value);	
 method Action iresp_ftype_ (Type value); 	 
 method Action iresp_dest_id_ (DestId value); 	
 method Action iresp_source_id_ (SourceId value); 	
 method Action iresp_tid_ (TranId value); 
 method Action iresp_ttype_ (Type value);
 method Action iresp_addr_ (Addr value); 	
 method Action iresp_byte_count_ (ByteCount value); 
 method Action iresp_byte_en_n_ (ByteEn value);
 method Action iresp_status_(Status value); 
 method Action iresp_local_(Bool value);
 */

endinterface



(* synthesize *)
(*always_enabled *)
(* always_ready*)

module mkGSM (Ifc_RapidIO) ; 

Reg#(InitiatorReqIfcPkt) initReqIfcPkt <- mkReg(defaultValue);
Wire#(Packet) wr_packetFromAgentReq <- mkDWire(defaultValue);
Reg#(Packet) rg_packetFromAgent <- mkReg(defaultValue); // this packet is for cache coherence operations.
Reg#(Packet) rg_packetToAgent  <- mkReg(defaultValue);
Wire#(Packet) wr_packetToAgentResp  <- mkDWire(defaultValue); //this packet is response from RapidIO. It will be sent to processor agent.
Wire#(Packet) wr_packetFromAgentAtRemote  <- mkDWire(defaultValue); // This packet will be received at remote as response.
Wire#(Packet) wr_packetFromRapidIOAtLocalResp <- mkDWire(defaultValue); // This packet will be initialized by initResIfc Signals.
Reg#(TranId ) rg_transID <- mkReg(0);





///     InitReq Wires   ////////////
Wire#(Bool) wr_ireq_sof <- mkDWire (False);
Wire#(Bool) wr_ireq_eof <- mkDWire (False);
Wire#(Bool) wr_ireq_vld <- mkDWire (False);
Wire#(Bool) wr_ireq_dsc <- mkDWire (False);
Wire#(Bool) wr_ireq_rdy_n <- mkDWire(True);


// -- Data
Wire#(TT) wr_ireq_tt <- mkDWire (0);
Wire#(Data) wr_ireq_data <- mkDWire (0);
Wire#(Bool) wr_ireq_crf <- mkDWire (False);
Wire#(Prio) wr_ireq_prio <- mkDWire (0);
Wire#(Type) wr_ireq_ftype <- mkDWire (0);
Wire#(DestId) wr_ireq_destid <- mkDWire (0);
Wire#(Addr) wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId) wr_ireq_tid <- mkDWire (0);
Reg#(TranId)  rg_ireq_tid <- mkReg (0);
Wire#(Type)  wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_ireq_byte_en <- mkDWire (0);

// Wires for optinal Signals -- Message
Wire#(Bool) wr_ireq_local <- mkDWire (False);
Wire#(DoorBell) wr_ireq_db_info <- mkDWire (0);
Wire#(MsgLen) wr_ireq_msg_len <- mkDWire (0);
Wire#(MsgSeg) wr_ireq_msg_seg <- mkDWire (0);
Wire#(Bit#(6)) wr_ireq_mbox <- mkDWire (0);
Wire#(Mletter) wr_ireq_letter <- mkDWire(0);

///      End InitReq Wires  ///////////////


/*
Bool treq_sof_n=False;
Bool treq_eof_n= False;
Bool treq_vld_n = False;
Bool treq_rdy_n = False;


Wire#(TT) wr_ireq_tt <- mkDWire (0);
Wire#(Data) wr_ireq_data <- mkDWire (0);
Wire#(Bool) wr_ireq_crf <- mkDWire (False);
Wire#(Prio) wr_ireq_prio <- mkDWire (0);
Wire#(Type) wr_ireq_ftype <- mkDWire (0);
Wire#(DestId) wr_ireq_destid <- mkDWire (0);
Wire#(Addr) wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId) wr_ireq_tid <- mkDWire (0);
Wire#(Type) wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn) wr_ireq_byte_en <- mkDWire (0);


Wire#(Bit#(64)) wr_treq_data <- mkDWire(0);
Wire#(Bit#(2))  wr_treq_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ftype <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_dest_id <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_tid <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ttype <- mkDWire(0);
Wire#(Bit#(34))  wr_treq_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_treq_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_treq_byte_en_n<- mkDWire(0);

*/
/*

Wire#(TT)      wr_ireq_tt <- mkDWire (0);
Wire#(Data)    wr_ireq_data <- mkDWire (0);
Wire#(Bool)    wr_ireq_crf <- mkDWire (False);
Wire#(Prio)    wr_ireq_prio <- mkDWire (0);
Wire#(Type)    wr_ireq_ftype <- mkDWire (0);
Wire#(DestId)  wr_ireq_destid <- mkDWire (0);
Wire#(Addr)    wr_ireq_addr <- mkDWire (0);
Wire#(Bit#(8)) wr_ireq_hopcount <- mkDWire (0);
Wire#(TranId)  wr_ireq_tid <- mkDWire (0);
Wire#(Type)    wr_ireq_ttype <- mkDWire (0);
Wire#(ByteCount) wr_ireq_byte_count <- mkDWire (0);
Wire#(ByteEn)    wr_ireq_byte_en <- mkDWire (0);

*/


//  Target request Signals  //
Wire#(Bool) wr_treq_sof_n <- mkDWire(True);
Wire#(Bool) wr_treq_eof_n <- mkDWire(True);
Wire#(Bool) wr_treq_vld_n <- mkDWire(True);
Wire#(Bool) wr_treq_rdy_n <- mkDWire(True);
Wire#(Bool) wr_treq_crf <- mkDWire(True);

Wire#(Data) wr_treq_data <- mkDWire(0);
Wire#(Bit#(2))  wr_treq_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_treq_ftype <- mkDWire(0);
Wire#(DestId)  wr_treq_dest_id <- mkDWire(0);
Wire#(SourceId )  wr_treq_src_id <- mkDWire(0);
Wire#(TranId)  wr_treq_tid <- mkDWire(0);
Wire#(Type)  wr_treq_ttype <- mkDWire(0);
Wire#(TT)    wr_treq_tt <- mkDWire(0);
Wire#(Addr)  wr_treq_addr <- mkDWire(0);
Wire#(ByteCount)  wr_treq_byte_count <- mkDWire(0);
Wire#(ByteEn)  wr_treq_byte_en_n<- mkDWire(0);


// Target Response Signals // 
Wire#(Bool) wr_tresp_sof_n <- mkDWire(True);
Wire#(Bool) wr_tresp_eof_n <- mkDWire(True);
Wire#(Bool) wr_tresp_vld_n <- mkDWire(True);
Wire#(Bool) wr_tresp_rdy_n <- mkDWire(True);
Wire#(Bool) wr_tresp_crf <-   mkDWire(False);
Wire#(Bool) wr_tresp_dsc_n <- mkDWire(False);

Wire#(Bool) variablesAssigned <- mkDWire(False);


Wire#(Bit#(64)) wr_tresp_data <- mkDWire(0);
Wire#(Bool) wr_tresp_no_data <- mkDWire(False);
Wire#(Bit#(2))  wr_tresp_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_tresp_ftype <- mkDWire(0);
Wire#(Bit#(32))  wr_tresp_dest_id <- mkDWire(0);
Wire#(Bit#(32))  wr_tresp_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_tresp_tid <- mkDWire(0);
Wire#(Type)  wr_tresp_ttype <- mkDWire(0);
Wire#(TT)  wr_tresp_tt <- mkDWire(0);

Wire#(Bit#(50))  wr_tresp_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_tresp_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_tresp_byte_en_n<- mkDWire(0);
Wire#(Bool)     wr_tresp_sof  <- mkDWire(True);
Wire#(Bit#(4))  wr_tresp_status <- mkDWire(0);
//Wire#(Mbox)  wr_tresp_mbox <- mkDWire(0);
Wire#(Mletter)  wr_tresp_letter <- mkDWire(0);

Wire#(MsgSeg) wr_tresp_msg_seg  <- mkDWire(0);
//Wire#(Bit#(32))  wr_tresp_destid <- mkDWire(0);





//  Initiator Response Signals //
Wire#(Bool)	wr_iresp_sof_n <- mkDWire(False);
Wire#(Bool) 	wr_iresp_eof_n <- mkDWire(False);
Wire#(Bool) 	wr_iresp_vld_n <- mkDWire(False);
Wire#(Bool)     wr_iresp_rdy_n <- mkDWire(False);

Wire#(TT)     wr_iresp_tt <- mkDWire(0);
Wire#(Status)   wr_iresp_status <- mkDWire(0);
Wire#(Bit#(64)) wr_iresp_data <- mkDWire(0);
Wire#(Bit#(2))  wr_iresp_prio <- mkDWire(0);
Wire#(Bit#(4))  wr_iresp_ftype <- mkDWire(0);
Wire#(Bit#(32))  wr_iresp_dest_id <- mkDWire(0);
Wire#(Bit#(32))  wr_iresp_src_id <- mkDWire(0);
Wire#(Bit#(8))  wr_iresp_tid <- mkDWire(0);
Wire#(Type)     wr_iresp_ttype <- mkDWire(0);
Wire#(Addr)  wr_iresp_addr <- mkDWire(0);
Wire#(Bit#(9))  wr_iresp_byte_count <- mkDWire(0);
Wire#(Bit#(8))  wr_iresp_byte_en_n<- mkDWire(0);
Wire#(Bool)     wr_iresp_crf     <- mkDWire(False);
Wire#(SourceId) wr_iresp_source_id <- mkDWire(0);
Wire#(Bool)     wr_iresp_local  <- mkDWire(False);



// 







Reg#(Bool) signalsSentOnInitReq_flag <- mkReg(False);
Reg#(Bool) initSignalsReady <- mkReg(False);
Reg#(Bool) initFlag <- mkReg(True);
Reg#(Bool) reqSent  <- mkReg(False);
Reg#(Bool) sendRequestRapidIO <- mkReg(False);
Reg#(Bool) isTgtRespReady <- mkReg(False);
Reg#(Bool ) rg_gariablesAssigned <- mkReg(False);
Reg#(Bool) isTgtReqSent <- mkReg(False);
Reg#(int)  selectProcessor <- mkReg(0);


(* descending_urgency = "rl_putRequestInitReq, rl_showIreqSignals"*)
(* descending_urgency = " rl_getSignalsInitResponse, rl_putRespSignalsTargetResInterface"*)

//(* execution_order = "rl_getReqTargetReqInterface,rl_putRespSignalsTargetResInterface" *)
// rule to put request on InitReq Interface. This rule will decide Ftype and Ttype based on requets from Processor Agent.
//(*mutually_exclusive = "rl_putRequestInitReq,rl_getSignalsInitResponse "*)
rule rl_putRequestInitReq;
	
	
	if(wr_packetFromAgentReq.validPacket == True)begin
		
		
		
		if(wr_packetFromAgentReq.reqType == Read_Miss)begin
		
			$display("Read Miss Received in GSM Module");
			
			wr_ireq_ftype <= 4'b0010;
			wr_ireq_ttype <= 4'b0100;
			
			
			// Data will be Invalid Becasue it is a read Miss request.
		
		end
		
		else if(wr_packetFromAgentReq.reqType == Write_Hit )begin //|| wr_packetFromAgentReq.reqType == Invalidate
			
			$display("Write Hit is Received in GSM Module");
			
			wr_ireq_ftype <= 4'b0010;
			wr_ireq_ttype <= 4'b1011;
			
			
			// This will not contain any data as, Its a Invalidate Request.
		
		end	


		else if (wr_packetFromAgentReq.reqType == Write_Miss)begin
				

		end
			
		
		
	wr_ireq_sof <= False;
	wr_ireq_eof <= False;
	wr_ireq_prio <= 0;
	wr_ireq_vld <= False;
	wr_ireq_data <= wr_packetFromAgentReq.data;
	wr_ireq_addr <= wr_packetFromAgentReq.physical_Address;
	
	wr_ireq_tid <= {	wr_packetFromAgentReq.pID,				// making TranId from coreID and last 4 bits of Physical address.	
				wr_packetFromAgentReq.physical_Address[3:0]};
	/*rg_ireq_tid <= {	wr_packetFromAgentReq.pID,				// making TranId from coreID and last 4 bits of Physical address.	
				wr_packetFromAgentReq.physical_Address[3:0]};*/
				
	//wr_ireq_rdy_n <= False;				
	
	//signalsSentOnInitReq_flag <= True;   // This makes rl_sendEOF to run. 
	//initSignalsReady <=True; // this variable making rule rl_decideDestID to run
	
	
	end
	


endrule

rule rl_showIreqSignals;
	
	if((wr_ireq_ftype) != 4'b0000 && (wr_ireq_ttype) != 4'b0000)begin
	$display("Ftype and Ttype has changed");
	$display("wr_ireq_ftype = %b, wr_ireq_ttype = %b ",wr_ireq_ftype,wr_ireq_ttype);
	$display("Sof=%b, eof = %b , wr_ireq_addr = %b , wr_ireq_data %b ", wr_ireq_sof, wr_ireq_eof, wr_ireq_addr,wr_ireq_data);
	$display("");
	//wr_packetFromAgentReq.validPacket <= False;
	end
endrule



/*
// This rule will send signals to other processors.
rule rl_decideDestID(initSignalsReady == True);
	//for(Integer i=0; i<3; i = i+1)begin
		
				rl_chkRequestLocal
		if(selectProcessor==0)begin
			wr_ireq_destid <= fromInteger(1);  // for processor 1
			sendRequest <= True;
			selectProcessor <= 1;
		end
		else if(selectProcessor==1)begin
			wr_ireq_destid <= fromInteger(2);	// for processor 2
			sendRequest <= True;
			selectmethod Action treq_sof_n_ (Bool value);
	wr_treq_sof_n <= value;
 endmethod
 method Action treq_eof_n_ (Boo value);
	wr_treq_eof_n <= value;
 endmethod
 method Action treq_vld_n_ (Bool value);
	wr_treq_vld_n <= value;
 endmethod
 method Bool _treq_rdy_n ();
	return wr_treq_rdy_n;
 endmethod

 method Action treq_tt_ (TT value); 
	wr_treq_tt <= value;
 endmethod 
 method Action treq_data_ (Data value);
	wr_treq_data <= value;
 endmethod
 method Action treq_crf_ (Bool value);
	wr_treq_crf <= value;
 endmethod
 method Action treq_prio_ (Prio value);
	wr_treq_prio <= value;
 endmethod
 method Action treq_ftype_ (Type value);
	wr_treq_ftype <= value;
 endmethod
 method Action treq_dest_id_ (DestId value);
	wr_treq_dest_id <= value;
 endmethod
 method Action treq_source_id_ (SourceId value);
	wr_treq_src_id <= value;
 endmethod
 method Action treq_tid_ (TranId value);
	wr_treq_tid <= value;
 endmethod
 method Action treq_ttype_ (Type value);
	wr_treq_ttype <= value;
 endmethod
 method Action treq_addr_ (Addr value);
	wr_treq_addr <= value;
 endmethod
 method Action treq_byte_count_ (ByteCount value);
	wr_treq_byte_count <= value;
 endmethod
 method Action treq_byte_en_n_ (ByteEn value);
	wr_treq_byte_en_n <= value;
 endmethodProcessor <= 2;
		end
			
		else if(selectProcessor==2)begin
			wr_ireq_destid <= fromInteger(3); // for processor 3
			sendRequest <= True;
			selectProcessor <= 3;
		end	
//	end
	
	initSignalsReady <= False;
endrule
*/

// This rule fire three times, for each processor.
//rule rl_sendRequest(sendRequestRapidIO);
		
/*	if(initReqIfc._InitReqIfc.ireq_rdy_n_ () == False)begin // Send Signals if InitReq interface is ready to receive
		initReqIfc._InitReqIfc._ireq_sof_n(wr_ireq_sof);
		initReqIfc._InitReqIfc._ireq_vld_n(wr_ireq_vld);
		initReqIfc._InitReqIfc._ireq_dsc_n(wr_ireq_dsc);
	//	initReqIfc._InitReqIfc.ireq_rdy_n_(True);
		initReqIfc._InitReqIfc._ireq_tt(wr_ireq_tt);
		initReqIfc._InitReqIfc._ireq_data(wr_ireq_data);
		initReqIfc._InitReqIfc._ireq_crf(wr_ireq_crf);
		initReqIfc._InitReqIfc._ireq_prio(wr_ireq_prio);
		initReqIfc._InitReqIfc._ireq_ftype(wr_ireq_ftype);
		initReqIfc._InitReqIfc._ireq_dest_id(wr_ireq_destid);
		initReqIfc._InitReqIfc._ireq_addr(wr_ireq_addr);
		initReqIfc._InitReqIfc._ireq_tid(wr_ireq_tid);
		initReqIfc._InitReqIfc._ireq_ttype(wr_ireq_ttype);
		sendRequestRapidIO <= False;
		
	end
	
	

endrule
*/
// Finally end of frame is asserted. Indicating last bit of frame has been transfered.

/*
rule rl_sendEOF(sendRequestRapidIO);

wr_ireq_eof <= True;
sendRequestRapidIO <= False;
signalsSentOnInitReq_flag <= True;
endrule
*/

//(signalsSentOnInitReq_flag )

rule rl_getSignalsInitResponse;
	

	 if(wr_iresp_vld_n == False &&  wr_iresp_sof_n == False )begin
	 	
	 	//if(wr_iresp_tid == rg_ireq_tid)begin // Transaction ID matched i.e. response of currentID
	 		
	 		
	 		if(wr_iresp_status == 4'b0001)begin // if request was ftype2 and ttype was 11 , remote cahce is invalidated.
	 		
 			rg_packetToAgent <= Packet{	// create packet 
 			
								pID: pack(wr_iresp_dest_id [31:28]),
								physical_Address: 50'b0,
								isRequest : False,
								resType : DONE,
								reqType : InValid,
								state   : (( wr_iresp_status == 4'b0)? Shared : ERROR),
								validPacket : True,
								data        : wr_iresp_data
								
	 			
	 			
	 					};
	 					
	 					//rg_treq_tid = 
	 		
	 		end
	 		
	 		else if(wr_ireq_ftype == 4'b0010 && wr_ireq_ttype <= 4'b0100 )begin // Read Miss Response, It will Contain data.
	
	 			rg_packetToAgent <= Packet{	
	 			
									pID: pack(wr_iresp_dest_id[31:28]),
									physical_Address: 50'b0,
									isRequest : False,
									resType : DONE,
									reqType : InValid,
									state   : ((wr_iresp_status == 4'b0)? InValid : ERROR),
									validPacket : True,
									data        :wr_iresp_data
									
	 			
	 			
	 						 };
	 		
	 			
	 		
	 		end
	 	
	 	 wr_packetFromRapidIOAtLocalResp <= rg_packetToAgent;
	 	 //signalsSentOnInitReq_flag <= False;
	 	//end
	 	
	 
	 end
	
	
	
endrule


////////////////// rl_getReqSignalsTgtInterface rule should be modified according to new Code //////////////////
// This rule will receive all signals from targetReqIfc interface.
/*
rule rl_getReqSignalsTgtInterface;
	

        	if( wr_treq_vld_n == False && wr_treq_sof_n == False)begin		
		
			
			
			wr_treq_data    <=  targetReqIfc._TgtReqIfc.treq_data_();
			wr_treq_prio    <= targetReqIfc._TgtReqIfc.treq_prio_();
			
			wr_treq_dest_id <= pack(targetReqIfc._TgtReqIfc.treq_source_id_()); //  Making source as destination for response
			//wr_treq_src_id  <= targetReqIfc._TgtReqIfc.
			wr_treq_tid         <= 	targetReqIfc._TgtReqIfc.treq_tid_();
			wr_treq_ttype       <=	targetReqIfc._TgtReqIfc.treq_ttype_();
			wr_treq_ftype       <=  targetReqIfc._TgtReqIfc.treq_ftype_();
			wr_treq_byte_count  <=  targetReqIfc._TgtReqIfc.treq_byte_count_();
			wr_treq_byte_en_n   <=  targetReqIfc._TgtReqIfc.treq_byte_en_n_();
			
		end
	


endrule

*/

//  After getting the signals from targetReqIfc, this rule interpretes the Signals and convert it to coherence packet.
rule rl_getReqTargetReqInterface;

		// matching whether request belongs to this node or not.
	if( wr_treq_sof_n == False &&  wr_treq_vld_n == False)begin
		
		
		if(wr_treq_ftype == 4'b0010 && wr_treq_ttype == 4'b1011)begin // Invalidate Case or write Hit.
			
			let pkt = Packet{
								pID: pack(wr_treq_dest_id[31:28]),
	 							physical_Address: wr_treq_addr,
	 							isRequest : True,
	 							resType : InValid,// Its a request.
	 							reqType : Read_Miss,
	 							state   : InValid,
	 							validPacket : True,
	 							data        : wr_treq_data	 						
			
					};
	



			//procesorAgentIfc ._inputs_Packet_RapidIO(pkt);
			wr_packetToAgentResp <= pkt;
			isTgtReqSent<= True;
				$display("Received wr_treq_ftype = %b,  wr_treq_ttype= %b",wr_treq_ftype,wr_treq_ttype);
				$display("Converted reqType = Read_Miss");			
				
		end
		
		

		
		else if( wr_treq_ftype == 4'b0010 &&  wr_treq_ttype == 4'b0100)begin // Read Miss Case
			
			let pkt = Packet{
								pID: pack(wr_treq_dest_id[31:28]),
	 							physical_Address: wr_treq_addr,
	 							isRequest : True,
	 							resType : InValid,// Its a request.
	 							reqType : Invalidate,
	 							state   : InValid,
	 							validPacket : True,
	 							data        : wr_treq_data
	 						
			
							};
	


			       $display("Received wr_treq_ftype = %b,  wr_treq_ttype= %b",wr_treq_ftype,wr_treq_ttype);
				$display("Converted reqType = Invalidate");
			wr_packetToAgentResp <= pkt;
			isTgtReqSent<= True;
			
			
		end
	
	//$display
	
	
	end



endrule



rule rl_putRespSignalsTargetResInterface; 

	
	
	if(wr_packetFromAgentAtRemote.validPacket == True)begin
	
		
		wr_tresp_addr      <= wr_packetFromAgentAtRemote.physical_Address;
		wr_tresp_sof_n     <= False;
		wr_tresp_vld_n     <= False;
		wr_tresp_dest_id   <= wr_treq_dest_id; // making 
		wr_tresp_data      <= wr_packetFromAgentAtRemote.data;
		wr_tresp_ftype     <= 4'b1101;                // Response Class 13
		wr_tresp_status    <= 0;
		wr_tresp_tid       <= wr_treq_tid; 
		
		
		
	
		isTgtRespReady <= True;	
		
		wr_tresp_rdy_n <= False;
	
		//$display("wr_tresp_addr = %b,  wr_tresp_ftype = %b ",wr_tresp_addr,wr_tresp_ftype);
		$display(" wr_tresp_ftype = 4'b1101 , wr_tresp_status  = DONE");
		//$display("wr_tresp_addr = %b, wr_tresp_data = %b",wr_packetFromAgentAtRemote.physical_Address,wr_packetFromAgentAtRemote.data);
		

	end


endrule

/*
rule rl_displayTargetResponse;
	
	if(wr_tresp_addr != 50'b0)begin
	$display("Signals responded from Remote");
		
			
	end
endrule
*/

/*
rule rl_putResponseTgtRespInterface(isTgtRespReady);

		if( targetRespIfc._TgtRespIfc.tresp_rdy_n_() == False)begin // Send Signals if Target Response interface is ready to receive
		
		

				
				wr_tresp_sof <= False;
				wr_tresp_vld_n <= False;
				wr_tresp_ttype <= wr_tresp_ttype;
				wr_tresp_ftype <= wr_tresp_ftype;
				wr_tresp_dest_id <= wr_tresp_dest_id;
				/*
				targetRespIfc._TgtRespIfc._tresp_sof_n(True);
				targetRespIfc._TgtRespIfc._tresp_vld_n(True);
	
				targetRespIfc._TgtRespIfc._tresp_ttype(wr_tresp_ttype);

//				targetRespIfc._TgtRespIfc._tresp_eof_n()
				targetRespIfc._TgtRespIfc._tresp_ftype(wr_tresp_ftype);
				targetRespIfc._TgtRespIfc._tresp_dest_id(wr_tresp_dest_id);
				targetRespIfc._TgtRespIfc._tresp_tid(wr_tresp_tid);
				
				targetRespIfc._TgtRespIfc._tresp_data(wr_tresp_data);
				
 

			sendRequestRapidIO <= False;
	
		end


endrule

*/





method Action _inputs_Packet_To_RapidIO_From_Agent_Request(Packet pkt);

wr_packetFromAgentReq <= pkt;

endmethod

method Action _inputs_From_Processor_Agent_To_GSM_At_Remote(Packet pkt);

wr_packetFromAgentAtRemote <= pkt; // wr_packetFromAgentAtRemote will be passed on TargetIfc.

endmethod

method Packet _outputs_Packet_RapidIO_Local_Resp();

return wr_packetToAgentResp; // Modifiy it.

endmethod



method Packet _outputs_Packet_RapidIO_Remote_Req();

return wr_packetToAgentResp;

endmethod


method Bool _outputs_ready_To_Receive_RapidIO();   // TODO This return value should be changed
return True;

endmethod




 // Method definition of  Request signals for RapidIO Module //////////////////
interface Ifc_GSM _ifc_gsm;

method Bool _ireq_sof_n();
	return (wr_ireq_rdy_n)?wr_ireq_sof:True;
endmethod
 

method Bool _ireq_eof_n();
	return (wr_ireq_rdy_n)?wr_ireq_eof:True;
endmethod 

method Bool _ireq_vld_n();
	return (wr_ireq_rdy_n)?wr_ireq_vld:True;
 endmethod
method Bool _ireq_dsc_n ();
	return wr_ireq_dsc;
endmethod 
// Ready
 method Action ireq_rdy_n_ (Bool value);
 	 wr_ireq_rdy_n <= value;
 endmethod

 method TT _ireq_tt();
	return wr_ireq_tt; 
 endmethod 
 
 method Data _ireq_data ();
	return wr_ireq_data;
 endmethod
 method Bool _ireq_crf ();
	return wr_ireq_crf;
 endmethod
 method Prio _ireq_prio ();
	return wr_ireq_prio;
 endmethod
 method Type _ireq_ftype();
	return wr_ireq_ftype;
 endmethod
 method DestId _ireq_dest_id ();
	return wr_ireq_destid; 
 endmethod 
 method Addr _ireq_addr();
	return wr_ireq_addr;
 endmethod 
 method Bit#(8) _ireq_hopcount ( );
	return wr_ireq_hopcount;
 endmethod
 method TranId _ireq_tid ();
	return wr_ireq_tid ;
 endmethod
 method Type _ireq_ttype ( );
	return wr_ireq_ttype ; 
 endmethod
 method ByteCount _ireq_byte_count ();
	return wr_ireq_byte_count; 
 endmethod
 method ByteEn _ireq_byte_en_n ();
	return wr_ireq_byte_en; 
 endmethod
 method Bool _ireq_local ();
	return wr_ireq_local; 
 endmethod
 method DoorBell  _ireq_db_info ();
	return wr_ireq_db_info; 
 endmethod
 method MsgLen _ireq_msg_len ();
	return wr_ireq_msg_len; 
 endmethod
 method MsgSeg _ireq_msg_seg ();
	return wr_ireq_msg_seg; 
 endmethod
 method Bit#(6) _ireq_mbox ( );
	return wr_ireq_mbox; 
 endmethod
 method Bit#(2) _ireq_letter ();
	return wr_ireq_letter; 
 endmethod

 


//////////////// Target Response as Output Methods ////////////


 method Bool outputs_tresp_sof_n_ ();
	return (!wr_tresp_rdy_n)?wr_tresp_sof_n: True;
 endmethod
 method Bool outputs_tresp_eof_n_ (); 
	return (!wr_tresp_rdy_n)?wr_tresp_eof_n : True;
 endmethod
 method Bool outputs_tresp_vld_n_ ();
	return (!wr_tresp_rdy_n)?wr_tresp_vld_n : True;
 endmethod
 method Bool outputs_tresp_dsc_n_ ();
	return (!wr_tresp_rdy_n)?wr_tresp_dsc_n : True;
 endmethod
 method Bool _inputs_tresp_rdy_n_();
	 return !(wr_tresp_rdy_n) ? wr_tresp_rdy_n : True; // 
 endmethod

 method TT outputs_tresp_tt_ (); 
	return wr_tresp_tt; 
 endmethod
 method Data outputs_tresp_data_ ();
	return wr_tresp_data;
 endmethod
 method Bool outputs_tresp_crf_ ();
	return wr_tresp_crf;
 endmethod
 method Prio outputs_tresp_prio_ ( );
	return wr_tresp_prio;
 endmethod
 method Type outputs_tresp_ftype_ ();
	return wr_tresp_ftype; 
 endmethod
 method DestId outputs_tresp_dest_id_ ();
	return wr_tresp_dest_id;
 endmethod
 method Status outputs_tresp_status_ ();
	return wr_tresp_status;
 endmethod
 method TranId outputs_tresp_tid_ ();
	return wr_tresp_tid ; 
 endmethod
 method Type outputs_tresp_ttype_ ();
	return wr_tresp_ttype; 
 endmethod
 method Bool outputs_tresp_no_data_ ();
	return wr_tresp_no_data; 
 endmethod

 method MsgSeg outputs_tresp_msg_seg_ ();
	return wr_tresp_msg_seg;
 endmethod
/*method Bit#(2) outputs_tresp_mbox_ ( );
	return wr_tresp_mbox ; 
 endmethod
*/
 method Mletter  outputs_tresp_letter_ ();
	return wr_tresp_letter; 
 endmethod
 
 
 
 
 
/////////////////    Initiator Response Signals // Called From Wrapper Module. ///

//-- Control Signal Interface
 method Action iresp_sof_n_ (Bool value);
 	wr_iresp_sof_n <= value;
 	
 	
 endmethod
 method Action iresp_eof_n_ (Bool value);
 	wr_iresp_eof_n <= value;
 endmethod
 

 
 
 method Action iresp_vld_n_ (Bool value);
	 wr_iresp_vld_n <= value;
 endmethod
 method Bool _iresp_rdy_n ();
 	return !wr_iresp_rdy_n ? wr_iresp_rdy_n : True;
 endmethod

 //-- Data Signal Interface
 method Action iresp_tt_ (TT value); 
 
 	wr_iresp_tt <= value;
 endmethod
 method Action iresp_data_ (Data value);
 
 	wr_iresp_data <=value;
 endmethod
 method Action iresp_crf_ (Bool value);
 	wr_iresp_crf <= value;
 endmethod
 
 method Action iresp_prio_ (Prio value);
 	wr_iresp_prio <= value;
 endmethod
 method Action iresp_ftype_ (Type value);
 	wr_iresp_ftype <= value;
 endmethod 
 method Action iresp_dest_id_ (DestId value);
 	wr_iresp_dest_id <= value;
 endmethod
 method Action iresp_source_id_ (SourceId value);
 	wr_iresp_source_id <= value;
 endmethod
 method Action iresp_tid_ (TranId value);
 
 	wr_iresp_tid <= value;
 endmethod
 method Action iresp_ttype_ (Type value);
 	wr_iresp_ttype <= value;
 endmethod
 method Action iresp_addr_ (Addr value);
 	wr_iresp_addr <= value;
 endmethod
 method Action iresp_byte_count_ (ByteCount value);
 	wr_iresp_byte_count <= value;
 endmethod
 method Action iresp_byte_en_n_ (ByteEn value);
 	wr_iresp_byte_en_n <= value;
 endmethod
 
 method Action iresp_status_(Status value);
 	wr_iresp_status <= value;
 endmethod
 
 method Action iresp_local_(Bool value);
 	wr_iresp_local <= value;
 endmethod




///////////////////  Target Request (Inputs Signals) //////////////////////


 
method Action treq_sof_n_ (Bool value);
	wr_treq_sof_n <= value;
 endmethod
 method Action treq_eof_n_ (Bool value);
	wr_treq_eof_n <= value;
 endmethod
 method Action treq_vld_n_ (Bool value);
	wr_treq_vld_n <= value;
 endmethod
 method Bool _treq_rdy_n ();
	return !wr_treq_rdy_n? wr_treq_rdy_n : True;
 endmethod

 method Action treq_tt_ (TT value); 
	wr_treq_tt <= value;
 endmethod 
 method Action treq_data_ (Data value);
	wr_treq_data <= value;
 endmethod
 method Action treq_crf_ (Bool value);
	wr_treq_crf <= value;
 endmethod
 method Action treq_prio_ (Prio value);
	wr_treq_prio <= value;
 endmethod
 method Action treq_ftype_ (Type value);
	wr_treq_ftype <= value;
 endmethod
 method Action treq_dest_id_ (DestId value);
	wr_treq_dest_id <= value;
 endmethod
 method Action treq_source_id_ (SourceId value);
	wr_treq_src_id <= value;
 endmethod
 method Action treq_tid_ (TranId value);
	wr_treq_tid <= value;
	//rg_treq_tid <= value;
 endmethod
 method Action treq_ttype_ (Type value);
	wr_treq_ttype <= value;
 endmethod
 method Action treq_addr_ (Addr value);
	wr_treq_addr <= value;
 endmethod
 method Action treq_byte_count_ (ByteCount value);
	wr_treq_byte_count <= value;
 endmethod
 method Action treq_byte_en_n_ (ByteEn value);
	wr_treq_byte_en_n <= value;
 endmethod

endinterface
/*
 method DoorBell treq_db_info_ ();
	
 endmethod
 method MsgLen treq_msg_len_ ();
	
 endmethod
 method MsgSeg treq_msg_seg_ ();
	
 endmethod
 method Bit#(6) treq_mbox_ ();
	return rio_MainCore._TargetReqInterface.treq_mbox_ ();
 endmethod
 method Mletter treq_letter_ ();
	return rio_MainCore._TargetReqInterface.treq_letter_ ();
 endmethod
*/


endmodule






endpackage
