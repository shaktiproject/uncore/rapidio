/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Logical Layer Concatenation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.1 IP Core Project
--
-- Description
-- This Module is to check the CRC32 code developed for the input data.
-- 1.Input to the data includes data appended with generated CRC16, zero padding if necessary and generated CRC32.
-- 2.This is connected to the CRC32 checker module.
--
-- To be done
-- 1.Crf yet to be implemented.
-- 2.Parameteristion of data width in each cycle and also width of rem value to be done later.
-- 
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013-2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package Tb_RapidIO_PhyCRC32Checker;

import RapidIO_PhyCRC32Checker::*;

(*synthesize*)
(*always_enabled*)
(*always_ready*)


module mkTb_RapidIO_PhyCRC32Checker(Empty);

Ifc_RapidIO_PhyCRC32Checker crc32_check <- mkRapidIO_PhyCRC32Checker;

Reg#(Bit#(16)) reg_ref_clk <- mkReg (0);

/*
-- Following rule, it is used to generate reference clock 
*/
rule rl_ref_clk_disp;
	reg_ref_clk <= reg_ref_clk + 1;
	$display (" \n---------------------------- CLOCK == %d ------------------------------", reg_ref_clk);
	if (reg_ref_clk == 10 )
		$finish (0);
endrule

rule r2(reg_ref_clk ==1); 
crc32_check._link_rx_sof_n (False);
crc32_check._link_rx_vld_n (False);
crc32_check._link_rx_data (128'h00000000c704dd7b0000000000000000);//data + code
crc32_check._link_rx_eof_n (False);
crc32_check._link_rx_rem (3'b000);
$display("Input 1== 128'h00000000000000000000000000066366");
$display("CRC Code == 32'hc704dd7b");
$display("REM value == 3'b000");
endrule:r2

rule r2_1(reg_ref_clk == 2); 
crc32_check._link_rx_sof_n (False);
crc32_check._link_rx_vld_n (False);
crc32_check._link_rx_data (128'h00000000000000006904bb5900000000);//data+code
crc32_check._link_rx_eof_n (False);
crc32_check._link_rx_rem (3'b001);
$display("Input 2 == 128'h000000000000000000000000000331b3");
$display("CRC Code == 32'h6904bb59");
$display("REM value == 3'b001");
endrule:r2_1

rule r2_2(reg_ref_clk ==3); 
crc32_check._link_rx_sof_n (False);
crc32_check._link_rx_vld_n (False);
crc32_check._link_rx_data (128'hcea0f52ad3e6efc11375a8d9c8ee587c);//data+code
crc32_check._link_rx_eof_n (True);
crc32_check._link_rx_rem (3'b111);
$display("Input 3_1 == 128'hcea0f52ad3e6efc11375a8d9c8ee587c");
$display("REM value == 3'b111");
endrule:r2_2

rule r2_3(reg_ref_clk ==4); 
crc32_check._link_rx_sof_n (True);
crc32_check._link_rx_vld_n (False);
crc32_check._link_rx_data (128'h67507a9569f377e089bad46ce4772c3e);//data+code
crc32_check._link_rx_eof_n (True);
crc32_check._link_rx_rem (3'b111);
$display("Input 3_2 == 128'h67507a9569f377e089bad46ce4772c3e");
$display("REM value == 3'b111");
endrule:r2_3

rule r2_4(reg_ref_clk ==5); 
crc32_check._link_rx_sof_n (True);
crc32_check._link_rx_vld_n (False);
crc32_check._link_rx_data (128'h33a83d4ab4f9bbf044dd000016b7cc6d);//data+code;//wrong input
crc32_check._link_rx_eof_n (False);
crc32_check._link_rx_rem (3'b011);
$display("Input 3_3 == 128'h33a83d4ab4f9bbf044dd6a36723b961f");
$display("CRC Code == 32'h16b7cc6c");
$display("REM value == 3'b011");
endrule:r2_4

rule r3;
$display("CRC Check=%b",crc32_check.output_CRC32_check_());
endrule:r3

endmodule:mkTb_RapidIO_PhyCRC32Checker
endpackage:Tb_RapidIO_PhyCRC32Checker
