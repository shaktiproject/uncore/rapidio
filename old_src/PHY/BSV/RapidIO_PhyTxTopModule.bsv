/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Physical Layer Top Module Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This module is developed to perform following operations:
-- 1. It behaves as the Top module for the RapidIO Physical layer. 
-- 2. It appends RapidIO CRC-16, Generates RapidIO Control packet, and, performs serialization and RapidIO lane stripping.
-- 3. The Transmit signal interfaces from User Interface are given as input to this module and output is obtained from lane stripping module.
-- 4. It follows 3 steps of operations: 
--    (1) CRC generation for the data packet transaction, 
--    (2) Control packet generation and 
--    (3) Strip the packets to 4 lane with each carries 64 bits. 
-- 
-- To Do's
-- This is a basic Physical layer module. Development is under progress. 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

package RapidIO_PhyTxTopModule;

import RapidIO_DTypes ::* ;
import RapidIO_PhyCRC16Generation ::* ;
import RapidIO_PhyLaneStripping ::* ;

interface Ifc_RapidIO_PhyTxTopModule;
// -- Input signals to Physical layer are received from RapidIO Logical/Transport layer
 method Action _input_LOGLayerTx_SOF_n (Bool value);
 method Action _input_LOGLayerTx_EOF_n (Bool value);
 method Action _input_LOGLayerTx_VLD_n (Bool value);
// method Action _input_LOGLayerTx_DSC_n (Bool value);
// method Bool _input_LOGLayerTx_RDY_n_ ();

 method Action _input_LOGLayerTx_DATA (DataPkt value);
 method Action _input_LOGLayerTx_REM (Bit#(4) value);
 method Action _input_LOGLayerTx_CRF (Bool value);
// method Action _input_LOGLayerTx_RESPONSE (Bool value);
// method Bool _input_LOGLayerTx_Resp_Only_ (); 

// -- Output signals are generated from lane stripping module
 method Bit#(64) outputs_Phylane1_();
 method Bit#(64) outputs_Phylane2_();
 method Bit#(64) outputs_Phylane3_();
 method Bit#(64) outputs_Phylane4_();

endinterface : Ifc_RapidIO_PhyTxTopModule


(* synthesize *)
(* always_enabled *)
(* always_ready *)
module mkRapidIO_PhyTxTopModule (Clock laneCLK, Reset laneRST, Ifc_RapidIO_PhyTxTopModule ifc); // Syntax used for multiple clock domains 

// -- Input Methods as Wires (For CRC Generation Module)
Wire#(Bool) wr_Tx_SOF <- mkDWire (False);
Wire#(Bool) wr_Tx_EOF <- mkDWire (False);
Wire#(Bool) wr_Tx_VLD <- mkDWire (False); 
Wire#(DataPkt) wr_InputPkt <- mkDWire (0);

// Input Methods as Registers (1 cycle delay for Lane Stripping)
Reg#(Bool) rg_Tx_SOF <- mkReg (False);
Reg#(Bool) rg_Tx_EOF <- mkReg (False);
Reg#(Bool) rg_Tx_VLD <- mkReg (False); 
Reg#(DataPkt) rg_InputPkt <- mkReg (0);

// -- Internal Wires and Registers
Wire#(DataPkt) wr_CRCAppendedPkt <- mkDWire (0);
Reg#(Type) rg_FtypeValue <- mkReg (0);
Reg#(Type) rg_TtypeValue <- mkReg (0);
Reg#(Bit#(2)) rg_TTValue <- mkReg (0);
Reg#(Bit#(4)) rg_Ftype11MsgLen <- mkReg (0);
Reg#(Bit#(16)) rg_CRCValue <- mkReg (0);

// -- Module Instantiation 
Ifc_RapidIO_PhyCRC16Generation rio_PhyCRCGeneration <- mkRapidIO_PhyCRC16Generation ();
Ifc_RapidIO_PhyLaneStripping rio_PhyLaneStripping <- mkRapidIO_PhyLaneStripping (laneCLK, laneRST);

/*
-- The FTYPE, TTYPE, MSGLEN and TT fields are delayed by a cycle.
-- The delay is given for Control packet generation.
-- Since these signals are valid during SOF, SOF is used as the control signal. 
*/   
rule rl_FType_TT_Determination_Activate (wr_Tx_VLD == True);
    if (wr_Tx_SOF == True) begin
        rg_FtypeValue <= wr_InputPkt[115:112];
        rg_TtypeValue <= (wr_InputPkt[117:116] == 2'b00) ? wr_InputPkt[95:92] : wr_InputPkt[79:76];
        rg_Ftype11MsgLen <= ((wr_InputPkt[115:112] == 'd11) && (wr_InputPkt[117:116] == 2'b00)) ? wr_InputPkt[95:92] : 
                            ((wr_InputPkt[115:112] == 'd11) && (wr_InputPkt[117:116] == 2'b01)) ? wr_InputPkt[79:76] : 0;
        rg_TTValue <= wr_InputPkt[117:116];
        end
endrule 

rule rl_display_Values;
    $display ("\nThe Ftype Value == %h", rg_FtypeValue);
    $display ("\nThe Ttype Value == %h", rg_TtypeValue);
    $display ("\nThe TT Value == %h", rg_TTValue);
endrule

/*
-- Similar to above activate rule, following rule deactivates the registers.
-- When the VLD signal is disabled which means no transaction is under progress, delaying registers are assigned with default value. 
*/   
rule rl_FType_TT_Determination_DeActivate (wr_Tx_VLD == False);
        rg_TtypeValue <= 0; 
        rg_FtypeValue <= 0; 
        rg_TTValue <= 0; 
endrule

/*
-- The CRC-16 value is stored in register before it is appended to the data packet.    
*/   
rule rl_CRC16_Append;
   rg_InputPkt <= (wr_Tx_VLD == True) ? wr_InputPkt : 0;
   rg_CRCValue <= rio_PhyCRCGeneration.outputs_CRC16_ ();
endrule

/*
-- Following rule is designed to append the CRC (16 bits) value to the packets. 
-- The FType, Ttype, TT and MsgLen values are stored in a temporary registers for CRC value appending. 
-- CRC value is added only during both EOF and VLD signals are valid (last data packet in the current transaction). 
-- Packets are not tampered during the CRC value addition. 
*/
rule rl_Appened_CRCto_DataPackets;
    if ((rg_Tx_VLD == True) && (rg_Tx_EOF == False)) begin // Except last packet in a transaction, the data packets are directly sent to lane stripping. 
        wr_CRCAppendedPkt <= rg_InputPkt; 
    end 
    else if ((rg_Tx_VLD == True) && (rg_Tx_EOF == True)) begin // When both Valid and EOF signal are asserted, the CRC value is added to data packets. 
        case (rg_FtypeValue) matches // Determines Ftype transaction
            'd2 : begin // Ftype 2 Read 
                if (rg_TTValue == 2'b00) // Dev8 ID
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:32], rg_CRCValue, rg_InputPkt[15:0]};
                else if (rg_TTValue == 2'b01) // Dev16 ID
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:16], rg_CRCValue};
                else
                    wr_CRCAppendedPkt <= 0; 
                end
            'd5 : begin // Ftype 5 Write 
                if (rg_TTValue == 2'b00) // Dev8 ID
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:96], rg_CRCValue, rg_InputPkt[79:0]};
                else if (rg_TTValue == 2'b01) // Dev16 ID
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:80], rg_CRCValue, rg_InputPkt[63:0]};
                else
                    wr_CRCAppendedPkt <= 0; 
                end
            'd6 : begin // Ftype Stream Write
                if ((rg_TTValue == 2'b00) || (rg_TTValue == 2'b01)) // Since both Dev8 and Dev16 have same format for the last packet, implementation is unique. 
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:64], rg_CRCValue, rg_InputPkt[47:0]};
                else 
                    wr_CRCAppendedPkt <= 0; 
                end
            'd8 : begin // Ftype 8 Maintain class
                if (rg_TTValue == 2'b00) begin // DevID 8
                    case (rg_TtypeValue) matches // Determines any one of the Transaction Types (Read Req, Write Req, Read Resp, and Write Resp)
                        'd0 : wr_CRCAppendedPkt <= {rg_InputPkt[127:48], rg_CRCValue, rg_InputPkt[31:0]}; // Read Request
                        'd1 : wr_CRCAppendedPkt <= {rg_InputPkt[127:112], rg_CRCValue, rg_InputPkt[95:0]}; // Write Request
                        'd2 : wr_CRCAppendedPkt <= {rg_InputPkt[127:112], rg_CRCValue, rg_InputPkt[95:0]}; // Read Response
                        'd3 : wr_CRCAppendedPkt <= {rg_InputPkt[127:48], rg_CRCValue, rg_InputPkt[31:0]}; // Write Response 
                        default : wr_CRCAppendedPkt <= 0; 
                    endcase
                    end
                else if (rg_TTValue == 2'b01) begin // DevID 16
                    case (rg_TtypeValue) matches
                        'd0 : wr_CRCAppendedPkt <= {rg_InputPkt[127:32], rg_CRCValue, rg_InputPkt[15:0]}; // Read Request
                        'd1 : wr_CRCAppendedPkt <= {rg_InputPkt[127:96], rg_CRCValue, rg_InputPkt[79:0]}; // Write Request
                        'd2 : wr_CRCAppendedPkt <= {rg_InputPkt[127:96], rg_CRCValue, rg_InputPkt[79:0]}; // Read Response
                        'd3 : wr_CRCAppendedPkt <= {rg_InputPkt[127:32], rg_CRCValue, rg_InputPkt[15:0]}; // Write Response
                        default : wr_CRCAppendedPkt <= 0; 
                    endcase
                    end
                else 
                    wr_CRCAppendedPkt <= 0; 
                end
            'd10 : begin // FType 10 DOORBELL 
                if (rg_TTValue == 2'b00) // DevID 8
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:64], rg_CRCValue, rg_InputPkt[47:0]};
                else if (rg_TTValue == 2'b01) // DevID 16
                    wr_CRCAppendedPkt <= {rg_InputPkt[127:48], rg_CRCValue, rg_InputPkt[31:0]};
                else
                    wr_CRCAppendedPkt <= 0; 
                end
            'd11 : begin // Ftype 11 Message Passing
                if (rg_TTValue == 2'b00) begin // DevID 8
                    if (rg_Ftype11MsgLen == 'd0) // Single packet 
                        wr_CRCAppendedPkt <= {rg_CRCValue, rg_InputPkt[111:0]};
                    else // Multiple Packet
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:64], rg_CRCValue, rg_InputPkt[47:0]};
                    end
                else if (rg_TTValue == 2'b01) begin // DevID 16
                    if (rg_Ftype11MsgLen == 'd0) // Single Packet
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:120], rg_CRCValue, rg_InputPkt[103:0]};
                    else // Multiple Packet 
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:56], rg_CRCValue, rg_InputPkt[39:0]};
                    end
                else
                    wr_CRCAppendedPkt <= 0;                 
                end 
            'd13 : begin // Ftype 13 Response 
                if (rg_TTValue == 2'b00) begin // Dev8 ID
                    if ((rg_TtypeValue == 'd0) || (rg_TtypeValue == 'd1)) // Response with No Data and Message Response 
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:80], rg_CRCValue, rg_InputPkt[63:0]};
                    else if (rg_TtypeValue == 'd8) // Response with Data
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:16], rg_CRCValue};
                    else 
                        wr_CRCAppendedPkt <= 0; 
                    end
                else if (rg_TTValue == 2'b01) begin // Dev16 ID
                    if ((rg_TtypeValue == 'd0) || (rg_TtypeValue == 'd1)) // Response with No Data and Message Response
                        wr_CRCAppendedPkt <= {rg_InputPkt[127:64], rg_CRCValue, rg_InputPkt[47:0]};
                    else if (rg_TtypeValue == 'd8) // Response with Data
                        wr_CRCAppendedPkt <= {rg_CRCValue, rg_InputPkt[111:0]};
                    end
                else
                    wr_CRCAppendedPkt <= 0; 
                end 
            default : wr_CRCAppendedPkt <= 0; 
        endcase
    end
    else
        wr_CRCAppendedPkt <= 0;
endrule 

/*
*/
rule rl_Connect_CRCPacket_LaneStripping;
    rio_PhyLaneStripping._link_tx_sof_n (!rg_Tx_SOF);
    rio_PhyLaneStripping._link_tx_eof_n (!rg_Tx_EOF);
    rio_PhyLaneStripping._link_tx_vld_n (!rg_Tx_VLD);
    rio_PhyLaneStripping._inputs_DataPkt(wr_CRCAppendedPkt);
//    rio_PhyLaneStripping._inputs_CtrlSymbolPkt(rio_PhyControlPktGen.outputs_ControlSymbolPkt_());
endrule

rule rl_display;
    $display ("\nThe Value of CRC Appended PAcket == %h", wr_CRCAppendedPkt);
endrule 

// Input and Output Methods Definition 
// -- Input Methods 
 method Action _input_LOGLayerTx_SOF_n (Bool value);
    rio_PhyCRCGeneration._input_link_tx_sof_n (value);
//    rio_PhyControlPktGen._inputs_link_tx_sof_n (value);
    wr_Tx_SOF <= !(value);
    rg_Tx_SOF <= !(value);
 endmethod 
 method Action _input_LOGLayerTx_EOF_n (Bool value);
    rio_PhyCRCGeneration._input_link_tx_eof_n (value);
//    rio_PhyControlPktGen._inputs_link_tx_eof_n (value);
    wr_Tx_EOF <= !(value); 
    rg_Tx_EOF <= !(value);
 endmethod 
 method Action _input_LOGLayerTx_VLD_n (Bool value);
    rio_PhyCRCGeneration._input_link_tx_vld_n (value);
//    rio_PhyControlPktGen._inputs_link_tx_vld_n (value);
    wr_Tx_VLD <= !(value);
    rg_Tx_VLD <= !(value);
 endmethod
// method Action _input_LOGLayerTx_DSC_n (Bool value);
// method Bool _input_LOGLayerTx_RDY_n_ ();

 method Action _input_LOGLayerTx_DATA (DataPkt value);
   rio_PhyCRCGeneration._input_link_tx_data (value); 
   wr_InputPkt <= value; 
 endmethod
 method Action _input_LOGLayerTx_REM (Bit#(4) value);
    rio_PhyCRCGeneration._input_link_tx_rem (value);
 endmethod
 method Action _input_LOGLayerTx_CRF (Bool value);
    rio_PhyCRCGeneration._input_link_tx_crf (value);
 endmethod
// method Action _input_LOGLayerTx_RESPONSE (Bool value);
// method Bool _input_LOGLayerTx_Resp_Only_ (); 

// -- Output signals are generated from lane stripping module
 method Bit#(64) outputs_Phylane1_();
    return rio_PhyLaneStripping.outputs_lane1_();
 endmethod
 method Bit#(64) outputs_Phylane2_();
    return rio_PhyLaneStripping.outputs_lane2_();
 endmethod
 method Bit#(64) outputs_Phylane3_();
    return rio_PhyLaneStripping.outputs_lane3_();
 endmethod
 method Bit#(64) outputs_Phylane4_();
    return rio_PhyLaneStripping.outputs_lane4_();
 endmethod

endmodule : mkRapidIO_PhyTxTopModule

endpackage : RapidIO_PhyTxTopModule
