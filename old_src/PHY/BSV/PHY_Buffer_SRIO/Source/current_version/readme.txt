RapidIOPhy_Buffer_Retrans_Deq::
As of now,the buffer module:
1. Accepts and stores the incoming packets according to priority.
2. When the threshold is achieved in any of the sections,Transmission begins and continues,that too according to priority.
3. In case of an error or if there is no sufficient space in recieve buffer to store the packets,transmission ceases.
4. If its possible to dequeue the accepted packets(all the packets till and including the ackid that was present in PA & all the packets till excluding 
the ackid that was present in PNA),retransmission of all packets starting from requested ackid is implemented.
5. Finally when receiver is ready, retransmission occurs and then normal transmission according to priority continues.


Dequeue strategy is implemented in the code but commented.Because if that is included,i.e.,if another similar lut is added,either the compilation 
process gets stuck or a stack space overflow occurs.To be solved in the future.