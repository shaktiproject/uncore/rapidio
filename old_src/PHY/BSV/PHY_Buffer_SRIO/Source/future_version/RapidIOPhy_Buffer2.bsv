/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Single register Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- 1.This module is developed for the Physical layer 
-- 2.A Single Register of 2310 bits is created to hold the incoming packet.
-- 3.This module behaves as the base for the set of each buffers(group of registers)
-- 4.In this module, write is carried out using shifting of data and read operation is by using the value of write counter.
--
-- Author(s):
-- M.Gopinathan (gopinathan18@gmail.com)
-- Ruby Kuriakose (ruby91adichilamackal3@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2015, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/


//Basic module for the set of 11 registers in the buffer which holds the packets using CReg



package RapidIOPhy_Buffer2;

import RapidIO_DTypes ::*;
import DefaultValue ::*;


interface Ifc_RapidIOPhy_Buffer2;
	method Action _tx_sof_n(Bool value);
	method Action _tx_eof_n(Bool value);
	method Action _tx_vld_n(Bool value);
	method Action _tx_data(DataPkt value);
	method Action _tx_rem(Bit#(4) value);
	method Action _tx_crf_n(Bool value);
	method Action _tx_deq(Bool value); //to enable dequeing. 
	method Action _tx_read(Bool value);//to enable reading.           
	method Action _tx_retransmsn(Bool value);//enables retransmission if equal to 1,i.e.,when RFR or a link request is sent by transmitter       
	method Action _tx_ack(Bit#(6) value);//6 bit ack id;actually ackid is 12 bits long but only lower 6 bits are given to packet 
 
	method Bool lnk_tsof_n_();
	method Bool lnk_teof_n_();
	method DataPkt lnk_td_();
	method Bool lnk_tvld_n_();
	method Bit#(4) lnk_trem_();
	method Bool lnk_tcrf_();
	method Bit#(1) valid_entry_();//indicates whether there is valid data
endinterface:Ifc_RapidIOPhy_Buffer2



(* synthesize *)
//(* always_enabled *)
//(* always_ready *)

module mkRapidIOPhy_Buffer2(Ifc_RapidIOPhy_Buffer2);

Wire#(Bool) wr_tx_sof_n <- mkDWire(True);
Wire#(Bool) wr_tx_eof_n <- mkDWire(True);
Wire#(Bool) wr_tx_vld_n <- mkDWire(True);
Wire#(DataPkt) wr_tx_data <- mkDWire(0);
Wire#(Bit#(4)) wr_tx_rem <- mkDWire(0);
Wire#(Bool) wr_tx_crf <- mkDWire(True);

Wire#(Bool) wr_lnk_tvld_n <- mkDWire(True);
Wire#(Bool) wr_lnk_tsof_n <- mkDWire(True);
Wire#(Bool) wr_lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) wr_lnk_td <- mkDWire(0);
Wire#(Bit#(4)) wr_lnk_trem <- mkDWire(0);
Wire#(Bool) wr_lnk_tcrf <- mkDWire(True);
Wire#(Bit#(6)) wr_tx_ack <- mkDWire(0);                
Wire#(Bool) wr_read_en <- mkDWire(False);
Wire#(Bool) wr_deq <- mkDWire(False);                 
Wire#(Bool) wr_retrans <- mkDWire(False);   

Reg#(BufferData) rg_buffer[2] <- mkCReg(2,defaultValue);//basic register to which data is written.total register size = 2310 bits.CReg is used, so that both read and write of ackid in the register is possible in one cycle itself. 
Reg#(Bit#(5)) rg_cnt_read <- mkReg(0);//read counter in a single register.       
Reg#(Bit#(5)) rg_cnt_write <- mkReg(0); //write counter indicating the position of each cycle of data in the register. 
Reg#(Bit#(5)) rg_cnt_wr_retrnsmsn <- mkReg(0);//to update write count register to initial value in case of retransmission.              

          



//writing into a register
rule r1_write(wr_tx_vld_n == False || wr_deq == True);                            
	if(wr_deq == True)
		begin
		rg_cnt_write <= 0;              
		rg_cnt_wr_retrnsmsn <= 0;		
		rg_cnt_read <= 0;
		rg_buffer[0] <= BufferData {tx_valid:0, tx_crf:True,tx_rem:0,tx_data:0};
		end
	
	else if(wr_tx_sof_n == False && wr_tx_eof_n == False)//only one cycle(128 bits) of data; 
		begin 
		rg_buffer[0] <= BufferData {tx_valid:1'b1,tx_crf:wr_tx_crf,tx_rem:wr_tx_rem,tx_data:{2176'b0,wr_tx_data}};//valid bit indicating data is present       
		rg_cnt_write <= rg_cnt_write +1; //write pointer incremented once if only single cycle of data             
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;	//for reloading in case of retransmission	
		rg_cnt_read <= 0; 
		end
	
	else if(wr_tx_sof_n == False)//Multiple cycle data
		begin   
		rg_buffer[0].tx_data[127:0] <= wr_tx_data;//first cycle always goes to position [127:0]         
		rg_cnt_write <= rg_cnt_write + 4; //pointer incremented by four in the first cycle in case of multiple cycles of data.                                                  
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 4;	//for reloading in case of retransmission	
		end

	else if(wr_tx_eof_n == False)
		begin
		let lv_rg_intrmediate = rg_buffer[0].tx_data[2175:0];
		rg_buffer[0] <= BufferData {tx_valid:1'b1,tx_crf:wr_tx_crf,tx_rem:wr_tx_rem,tx_data:{wr_tx_data,lv_rg_intrmediate}}; //last cycle always goes to position [2303:2176]; [2308:2304] stores crf and rem in 5 bits
		rg_cnt_read <= 0; 
		end 

	else 
		begin
		let lv_rg_shift_in_data = rg_buffer[0].tx_data[2175:128];//(local register is of 2048 bits excluding initial 8 bits and storage area of first cycle and last cycle of data.) 
		lv_rg_shift_in_data = lv_rg_shift_in_data >> 128; //data is first written to msb of local register and then shifted to right whenever next 128 bits come.  
		lv_rg_shift_in_data[2047:1920]= lv_rg_shift_in_data[2047:1920] | wr_tx_data; //data is first written to msb of local register  
		rg_buffer[0].tx_data[2175:128]<= lv_rg_shift_in_data;
		rg_cnt_write <= rg_cnt_write +1;
		rg_cnt_wr_retrnsmsn <= rg_cnt_wr_retrnsmsn + 1;		
		end  
endrule


//for reloading write and read counters so that txmsn starts from the beginning of a packet in case of retransmission
rule retxmsn(wr_retrans == True);
	rg_cnt_write <= rg_cnt_wr_retrnsmsn;
	rg_cnt_read <= 0;
endrule


//AckID assignment to port 0 of the register				
rule r1_ackid_assnmnt(wr_read_en == True && rg_cnt_read == 0);
	if(rg_cnt_write == 1)
		begin
		rg_buffer[0].tx_data[127:122] <= wr_tx_ack;
		$display("ACKID assigning....");
		end
	else if(rg_cnt_write != 0 && rg_cnt_write != 1 && rg_cnt_write != 2)
		begin
		rg_buffer[0].tx_data[127:122] <= wr_tx_ack;         
		$display("Ackid assigning....");
		end
endrule


//reading enabled by wr_read
rule r1_read_1(wr_read_en == True && (rg_cnt_write == 1 || (rg_cnt_write != 0 && rg_cnt_write != 1 && rg_cnt_write != 2)));                 
	if(rg_cnt_write == 1) //write counter = 1 implies single cycle of data 
		begin
		if(rg_cnt_read == 0)
			begin
			wr_lnk_tsof_n <= False;
			wr_lnk_teof_n <= False;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[127:0];
			wr_lnk_trem <= rg_buffer[1].tx_rem;
			wr_lnk_tcrf <= rg_buffer[1].tx_crf;
			rg_cnt_read <= 0;
			rg_cnt_write <= 0;
			end
		end

	else if(rg_cnt_write != 0 && rg_cnt_write != 1 && rg_cnt_write != 2)//Multiple cycle of Data
		begin
		if(rg_cnt_read == 0)
			begin
			wr_lnk_tsof_n <= False;//AckID assigning and reading of first beat of data happens in same cycle
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[127:0];
			rg_cnt_write <= rg_cnt_write-2;//for satisfying the conditions given while writing since ackid assigning and reading now happens in same cycle
			rg_cnt_read <= rg_cnt_read + 1;
			end
		else if(rg_cnt_write == 18)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[255:128];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 17)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[383:256];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 16)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[511:384];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 15)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[639:512];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 14)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[767:640];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 13)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[895:768];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 12)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1023:896];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 11)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1151:1024];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 10)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1279:1152];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 9)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1407:1280];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 8)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1535:1408] ;
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 7)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1663:1536];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 6)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1791:1664];
			rg_cnt_read <= rg_cnt_read +1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 5)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[1919:1792];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 4)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[2047:1920];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end

		else if(rg_cnt_write == 3)
			begin
			wr_lnk_tsof_n <= True;
			wr_lnk_teof_n <= True;
			wr_lnk_tvld_n <= False;
			wr_lnk_td <= rg_buffer[1].tx_data[2175:2048];
			rg_cnt_read <= rg_cnt_read + 1;
			rg_cnt_write <= rg_cnt_write - 1;
			end
		end
endrule


rule r1_read_2(wr_read_en == True && rg_cnt_write == 2 && rg_cnt_read != 0);
		wr_lnk_teof_n <= False;
		wr_lnk_tsof_n <= True;
		wr_lnk_tvld_n <= False;
		wr_lnk_td <= rg_buffer[1].tx_data[2303:2176];
		wr_lnk_trem <= rg_buffer[1].tx_rem;
		wr_lnk_tcrf <= rg_buffer[1].tx_crf;
		rg_cnt_read <= 0;
endrule


//Input Methods
method Action _tx_ack(Bit#(6) value);
	wr_tx_ack <= value;
endmethod

method Action _tx_sof_n(Bool value);
     wr_tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     wr_tx_eof_n <= value;
endmethod

method Action _tx_crf_n(Bool value);
     wr_tx_crf <= value;
endmethod

method Action _tx_vld_n(Bool value);
     wr_tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     wr_tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     wr_tx_rem <= value;
endmethod

method Action _tx_deq(Bool value);
     wr_deq <= value;
endmethod

method Action _tx_read(Bool value);
     wr_read_en <= value;
endmethod

method Action _tx_retransmsn(Bool value);
	wr_retrans <= value;
endmethod


//Output Methods
method Bool lnk_tsof_n_();
     return wr_lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return wr_lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return wr_lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return wr_lnk_trem;
endmethod

method Bool lnk_tcrf_();
      return wr_lnk_tcrf;
endmethod

method Bool lnk_tvld_n_();
     return wr_lnk_tvld_n;
endmethod

method Bit#(1) valid_entry_();
      return rg_buffer[0].tx_valid;
endmethod

endmodule:mkRapidIOPhy_Buffer2
endpackage:RapidIOPhy_Buffer2
