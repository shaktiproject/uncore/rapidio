package RapidIOPhy_Buffer_rd_wrt_ptr;


import AckId_generator::*;

interface Ifc_RapidIOPhy_Buffer_rd_wrt_ptr;

	method Action _in_wrt_ptr(Bit#(4) value);
	method Action _in_rd_ptr(Bit#(4) value); 

	method Bit#(1) tx_read_en_();
	method Bit#(4) read_ptr_init_();

endinterface:Ifc_RapidIOPhy_Buffer_rd_wrt_ptr

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer_rd_wrt_ptr(Ifc_RapidIOPhy_Buffer_rd_wrt_ptr);


//Ifc_AckId_generator a1 <- mkAckId_generator;

Wire#(Bit#(4)) wr_wrt_ptr <- mkDWire(0);
Wire#(Bit#(4)) wr_read_ptr <- mkDWire(0);
Reg#(Bit#(1)) rg_read <- mkReg(0);
Reg#(Bit#(4)) rg_ptr <- mkReg(0);

rule r1_rd_ptr(wr_wrt_ptr >= 4 && wr_read_ptr == 0);

//a2._ack_en(False);
//$display("wrt_ptr =%b",wr_wrt_ptr);
//$display("read_en =%b",rg_read);

	rg_read <= 1'b1;
	rg_ptr <= 4'b0001;
endrule

rule disp;
	$display("read_en =%b",rg_read);
endrule

method Action _in_wrt_ptr(Bit#(4) value);
	wr_wrt_ptr <= value;
endmethod

method Action _in_rd_ptr(Bit#(4) value); 
	wr_read_ptr <= value;
endmethod

method Bit#(1) tx_read_en_();
	return rg_read;
endmethod

method Bit#(4) read_ptr_init_();
	return rg_ptr;
endmethod
	

endmodule:mkRapidIOPhy_Buffer_rd_wrt_ptr
endpackage:RapidIOPhy_Buffer_rd_wrt_ptr
