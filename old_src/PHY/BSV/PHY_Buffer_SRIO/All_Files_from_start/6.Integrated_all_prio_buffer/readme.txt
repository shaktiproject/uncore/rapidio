6.1 - Normal Reg is used in case of basic register so that ackid assignment and reading cannot happen in the same cycle and therefore delay is caused.  For compensating this, early enabling of the next section to be read and assigning ackid is tried.

6.2 - Txmtng of first cycle and ackid assignment happens in same cycle using CReg(both writing and reading possible in same cycle). Thus continuous transmission of data takes place without any delay. So this is the final version till now.
