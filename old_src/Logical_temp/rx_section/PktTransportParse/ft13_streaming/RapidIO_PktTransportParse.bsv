/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Incoming Packet Parsing Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. After the incoming packets are separated as Header and Data Packets, it is forwarded 
--    to this module for further processing.
-- 2. The Header and Data packets are analyzed and logical layer ftype packets are generated.
-- 3. This module invokes RxFtypeFunctions package to depacketize the incoming packets.
-- 4. Supports both Dev8 and Dev16 fields. 
--
-- To Do's
-- Yet to Complete 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
--	-- Packet Description (Dev8 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:0], 8'h00 }
	Data_2 -> { Data2, 64'h0 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[7:0], SrcID[7:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--      -- Packet Description (Dev16 Support) -- 
--
-- The incoming packets for different format type (FType) are shown below. 

1. Request Class (Atomic or NRead Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0] }

2. Write Request Class (Atomic or NWRITE Request)
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], srcTID[7:0], Address[28:0], WdPtr, Xamsbs[1:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

3. Stream Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], Address[28:0], Resv, Xamsbs[1:0], Data1[63:8] }
	Data_1 -> { Data1[7:0], Data2 } 
	Data_2 -> { 8'h00, Data3 } // This packet will extend, if there are more data but the number of bytes in the packet should not exceed 80 bytes (10 Packets)

4. Maintenance Read Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], rdsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], wrsize[3:0], TgtTID[7:0], Hop_Count[7:0], Config_Offset[20:0], WdPtr, Resv, Data[63:24] }
	Data_1 -> { Data[23:0] }

6. Maintenance Read Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0], Data[63:24] }
	Data_1 -> { Data[23:0] }

7. Maintenance Write Response 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Hop_Count[7:0], Resv[23:0] }

8. Response With Data 
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0], Data[63::0] }

9. Response Without Data
	Header -> { prio[1:0], tt[1:0], ftype[3:0], destID[15:0], SrcID[15:0], ttype[3:0], status[3:0], TgtTID[7:0] }

--------------------------------------------------------------------------------------------------------------------------------------------------------
--
--        
*/

package RapidIO_PktTransportParse;

import RapidIO_DTypes ::*;
import RapidIO_FTypeDfns ::*;
import DefaultValue ::*;
import RapidIO_RxFTypeFunctionsDev8 ::*;
import RapidIO_RxFTypeFunctionsDev16 ::*;
import RapidIO_InComingPkt_Separation ::*;
import RapidIO_TgtDecoder_ByteCnt_ByteEn ::*;


`include "RapidIO.defines"

/*
-- Incoming Signals are same as the Transmitter signals.
-- SOF, EOF, Valid, Ready, Data (Packet), RxRem, CRF. 
-- Input Signals are given through Method Action 
-- Output Signals are obtained through Method return. 
--
-- The Incoming packet is decoded and obtained at the output 
-- in Ftype format packets. 
-- Data also decoded and resulted separately. 
*/


interface Ifc_RapidIO_PktTransportParse;
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
 method Action _PktParseRx_EOF_n (Bool value);
 method Action _PktParseRx_VLD_n (Bool value);
 method Bool link_rx_rdy_n_ ();

 method Action _PktParseRx_data (DataPkt value);
 method Action _PktParseRx_rem (Bit#(4) value);
 method Action _PktParseRx_crf (Bool value);

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);

 // Output Ports as Methods

 method Maybe#(FType13_ResponseClass) outputs_RxFtype13ResponseClass_ ();
 method Maybe#(Data) outputs_RxFtype13ResponseData_ ();
 method ReceivedPktsInfo outputs_ReceivedPkts_ ();

 /*
 -- The Transport Layer fields are decoded and removed from the packet and sent to Logical layer 
 -- outputs directly. 
 */
 method TT outputs_TTReceived_ ();
 method DestId outputs_RxDestId_ ();
 method SourceId outputs_RxSourceId_ ();
 method Prio outputs_RxPrioField_ ();
 method Bit#(4) outputs_MaxPktCount_ ();

endinterface : Ifc_RapidIO_PktTransportParse

/*
-- This type is defined to concatenate the Header packet, Data packet and Packet count for other modules for computation  
*/
typedef struct { DataPkt headerpkt;
		 DataPkt datapkt;
		 Bit#(4) pktcount;
		 Bool lastpkt;
} ReceivedPktsInfo deriving (Bits, Eq);

instance DefaultValue#(ReceivedPktsInfo); // Default Value assigned for the Received PktsInfo 
   defaultValue = ReceivedPktsInfo {headerpkt: 0, datapkt: 0, pktcount: 0, lastpkt: False };
endinstance


(* synthesize *)
//(* always_enabled *)
//(* always_ready *)
module mkRapidIO_PktTransportParse (Ifc_RapidIO_PktTransportParse);

 // Input Methods as Wires 
Wire#(Bool) wr_PktParseRx_SOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_EOF <- mkDWire (False);
Wire#(Bool) wr_PktParseRx_VLD <- mkDWire (False);

Wire#(DataPkt) wr_PktParseRx_data <- mkDWire (0);
Wire#(Bit#(4)) wr_PktParseRx_rem <- mkDWire (0);
Wire#(Bool) wr_PktParseRx_crf <- mkDWire (False);

// Internal Wires and Registers
Wire#(DataPkt) wr_HeaderPkt <- mkDWire (0);
Reg#(DataPkt) rg_HeaderPkt <- mkReg (0); // Delay HeaderPkt for 1 clock cycle
Wire#(DataPkt) wr_DataPkt <- mkDWire (0);
Reg#(DataPkt) rg_DataPkt <- mkReg (0); // Delay DataPkt for 1 Clock Cycle
Wire#(Bit#(4)) wr_PktCount <- mkDWire (0);
Reg#(Bit#(4)) rg_PktCount <- mkReg (0);// Delay Pkt Count for 1 Clock Cycle
Reg#(Bool) rg_LastPkt <- mkReg (False); // Delayed the Last Packet valid bit 

Reg#(TT) rg_TTReceived <- mkReg (0); 
Reg#(Prio) rg_PrioReceived <- mkReg (0);
Reg#(DestId) rg_DestIDReceived <- mkReg (0);
Reg#(SourceId) rg_SrcIDReceived <- mkReg (0);
Reg#(Bit#(8)) rg_HopCountReceived <- mkReg (0);
Reg#(Maybe#(Bit#(4))) rg_RxRem <- mkReg (tagged Invalid);

Reg#(DataPkt) rg_datapacket <- mkReg (0); // Delay DataPkt for 1 Clock Cycle
Reg#(DataPkt) rg_datapacket_delay <- mkReg (0);
Reg#(DataPkt) rg_datapacket_delay_2 <- mkReg (0);

Reg#(DataPkt) rg_HeaderPktFtype13 <- mkReg (0);
Reg#(DataPkt) rg_HeaderPktFtype13_delay <- mkReg (0);
// Ftype Resisters to hold the logical ftype packets 
Reg#(Maybe#(FType13_ResponseClass)) rg_Ftype13_ResponseClass <- mkReg (tagged Invalid);

Wire#(Bool) wr_TxReady_In <- mkDWire (False);


// Module Instantiation
Ifc_RapidIO_InComingPkt_Separation pkt_Separation <- mkRapidIO_InComingPkt_Separation ();
//Ifc_RapidIO_TgtDecoder_ByteCnt_ByteEn tgtDecoder_ByteCnt_ByteEn <- mkRapidIO_TgtDecoder_ByteCnt_ByteEn ();

/*
-- Following rule, Used to send the incoming packets to Incoming Separation Module to separate the incoming packets 
-- as Header and Data Packet. 
-- The output is obtained and stored them in both Registers and Wires. 
*/
rule rl_FromIncomingPktSeparation;
    	wr_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ ();
    	rg_HeaderPkt <= pkt_Separation.outputs_HeaderPkt_ (); // Delay the Header Packet
    	wr_DataPkt <= pkt_Separation.outputs_DataPkt_ ();
    	rg_DataPkt <= pkt_Separation.outputs_DataPkt_ (); // Delay the Data Packet 
    	wr_PktCount <= pkt_Separation.outputs_PktCount_ ();

    	rg_LastPkt <= pkt_Separation.outputs_LastPkt_ (); // Delayed the Last Packet valid bit 
endrule

rule header_data_packet;
        rg_HeaderPktFtype13 <= wr_HeaderPkt;
	    rg_HeaderPktFtype13_delay <= rg_HeaderPktFtype13;
		//rg_HeaderPktFtype6_delay_2 <= rg_HeaderPktFtype6_delay;

    	rg_PktCount <= pkt_Separation.outputs_PktCount_ (); // Delay the Packet Count 
		rg_datapacket <= wr_DataPkt;
		rg_datapacket_delay <= rg_datapacket;
		rg_datapacket_delay_2 <= rg_datapacket_delay;
endrule
/*
rule display;
	$display("\nwire header == %h ",wr_HeaderPkt);
	$display("register header == %h",rg_HeaderPktFtype13);
	$display("wire data == %h ",wr_DataPkt);
	$display("register data == %h ",rg_datapacket);
$display("register packet count == %h \n",rg_PktCount);
	$display("register data delay == %h",rg_datapacket_delay);
endrule

/*
-- Rule is used to decode the Destination ID, Source ID and Prio Bit information from the 
-- incoming packets (Transport Layer Fields)
-- Using TT field in the received packet, Dev8 or Dev16 is determined. 
-- If TT == 00, Dev8 and If TT == 01, Dev16 
*/
rule rl_HeaderDecode_IncomingPkt;
    if (pkt_Separation.outputs_PktCount_ () == 0) begin
	rg_DestIDReceived <= 0;
	rg_SrcIDReceived <= 0;
	rg_PrioReceived <= 0;
	rg_TTReceived <= 0; 
    end
    else begin
      	rg_TTReceived <= wr_HeaderPkt[117:116]; 
      	rg_PrioReceived <= wr_HeaderPkt[119:118];
      	if (wr_HeaderPkt[117:116] == 'b00) begin // will be corrected XXXXXXXXXXXXXXXXXXXXXXXXX 2'b00 for 8 bit device
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'h0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'h0};
      	end
      	else if (wr_HeaderPkt[117:116] == 'b01) begin
		rg_DestIDReceived <= {wr_HeaderPkt[111:96], 16'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[95:80], 16'd0};
      	end 
      	else begin 
		rg_DestIDReceived <= {wr_HeaderPkt[111:104], 24'd0};
		rg_SrcIDReceived <= {wr_HeaderPkt[103:96], 24'd0};
      	end
    end 
endrule

/*
-- Rule to decode the hop count value for the maintenance packet
*/
rule rl_HopCountDecode;
    if ((wr_HeaderPkt[115:112] == `RIO_FTYPE8_MAINTAIN) && (wr_PktCount != 0))
	rg_HopCountReceived <= wr_HeaderPkt[79:72];
    else 
	rg_HopCountReceived <= 0;	
endrule

/*
-- Rule is to determine the values of the RxRem.
-- RxRem is valid only when the eof is enabled  
-- Physical layer (2 bytes ) removed and rg_RxRem contains the value of the RxRem for the logical layer
*/
rule rl_RxRemValid;
    if (wr_PktParseRx_EOF == True)
	rg_RxRem <= tagged Valid (wr_PktParseRx_rem - 'd2);
    else 
	rg_RxRem <= tagged Invalid;
endrule



Reg#(Bit#(64)) rg_Dev8TempRespData <- mkReg (0);
Reg#(Bit#(8)) rg_Dev16TempRespData <- mkReg (0); 
Reg#(Maybe#(Data)) rg_RespClassData <- mkReg (tagged Invalid);


rule rl_Ftype13ResponseFormat; // Format Type 13

    Bit#(40) lv_Dev8TempRespData = 0;
    Bit#(56) lv_Dev16TempRespData = 0; 
    Data lv_Dev8RespData = 0;
    Data lv_Dev16RespData = 0; 

    if (wr_HeaderPkt[115:112] == `RIO_FTYPE13_RESPONSE) begin
        if (wr_HeaderPkt[117:116] == 2'b00) begin // Dev8
            

if ((wr_HeaderPkt[95:92] == 'd8) && (rg_PktCount == 'd1)) begin
	lv_Dev8RespData = {rg_HeaderPktFtype13[79:0],wr_DataPkt[127:80]};
//$display("0000000000000000000000000000000000000000000000000000000000000000000000000000000");
end

else if ((rg_HeaderPktFtype13[95:92] == 'd8) && (rg_PktCount == 'd2)) begin
	lv_Dev8RespData = {rg_datapacket[79:0],wr_DataPkt[127:80]};
end

else if ((rg_HeaderPktFtype13[95:92] == 'd8) && (rg_PktCount > 'd2)) begin
	lv_Dev8RespData = {rg_datapacket[79:0],wr_DataPkt[127:80]};
//$display("11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111");
end

	//$display("\nlocal variable data == %h\n",lv_Dev8RespData);

	    if ((wr_HeaderPkt[95:92] == 'd0) && (rg_PktCount == 'd1)) begin // Response Without Data 
		rg_Ftype13_ResponseClass <= tagged Valid fn_Dev8Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt, wr_HeaderPkt[95:92], 0);
		rg_RespClassData <= tagged Invalid;
            end 
else if ((wr_HeaderPkt[95:92] == 'd8) && (rg_PktCount == 'd0))
rg_RespClassData <= tagged Invalid;
	    else if ((wr_HeaderPkt[95:92] == 'd8) && (rg_PktCount == 'd1)) begin // Response With Data 
		rg_Ftype13_ResponseClass <= tagged Valid fn_Dev8Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt, rg_HeaderPktFtype13[95:92], lv_Dev8RespData);
		rg_RespClassData <= tagged Valid lv_Dev8RespData;
//$display("222222222222222222222222222222222222222222222222222222222222222222222222222");
            end

else if ((wr_HeaderPkt[95:92] == 'd8) && (wr_PktCount == 'd2)) begin
rg_Ftype13_ResponseClass <= tagged Valid fn_Dev8Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt,rg_HeaderPktFtype13[95:92], lv_Dev8RespData);
		rg_RespClassData <= tagged Valid lv_Dev8RespData;
		end
else if ((wr_HeaderPkt[95:92] == 'd8) && (rg_PktCount >'d2)) begin
rg_Ftype13_ResponseClass <= tagged Valid fn_Dev8Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt,rg_HeaderPktFtype13[95:92], lv_Dev8RespData);
		rg_RespClassData <= tagged Valid lv_Dev8RespData;

//$display("333333333333333333333333333333333333333333333333333333333333333333333333333333333");
end

	    else begin
		rg_RespClassData <= tagged Invalid;
		rg_Ftype13_ResponseClass <= tagged Invalid;
            end
        end 


        else if (wr_HeaderPkt[117:116] == 2'b01) begin // Dev16 
            // Response Data Generation . . .
            lv_Dev16RespData = (wr_PktCount == 'd1) ? {wr_HeaderPkt[63:0],64'b0} : 0;

	    if ((wr_HeaderPkt[79:76] == 'd0) && (wr_PktCount == 'd1)) begin // Response Without Data 
		rg_Ftype13_ResponseClass <= tagged Valid fn_Dev16Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt, 0);
		rg_RespClassData <= tagged Invalid;
            end 
	    else if ((wr_HeaderPkt[79:76] == 'd8) && (wr_PktCount == 'd1)) begin // Response With Data
		rg_Ftype13_ResponseClass <= tagged Valid fn_Dev16Ftype13ResponsePkt (wr_PktCount, wr_HeaderPkt, wr_DataPkt, lv_Dev16RespData);
		rg_RespClassData <= tagged Valid lv_Dev16RespData;
            end
	    else begin
		rg_RespClassData <= tagged Invalid;
		rg_Ftype13_ResponseClass <= tagged Invalid;
            end

        end 
    end
    else begin
	rg_RespClassData <= tagged Invalid;
	rg_Ftype13_ResponseClass <= tagged Invalid;
    end
endrule

rule display1;
	$display("\npacket count == %h",wr_PktCount);
	//$display("wire header packet == %h",wr_HeaderPkt);
//	$display("wire data packet == %h",wr_DataPkt);

endrule

// Module Definition 
 // Input Ports as Methods
 method Action _PktParseRx_SOF_n (Bool value); 
	pkt_Separation._inputs_SOF (!value);
 endmethod
 method Action _PktParseRx_EOF_n (Bool value);
	pkt_Separation._inputs_EOF (!value);
 endmethod
 method Action _PktParseRx_VLD_n (Bool value);
	pkt_Separation._inputs_VLD (!value);
 endmethod
/* method Bool link_rx_rdy_n_ (); // Need to Implement
	return (wr_TxReady_In); 
 endmethod*/

 method Action _PktParseRx_data (DataPkt value);
	pkt_Separation._inputs_DataPkt (value);
 endmethod

 method Action _PktParseRx_rem (Bit#(4) value); 
	wr_PktParseRx_rem <= value; 
 endmethod

 method Action _PktParseRx_crf (Bool value); 
	wr_PktParseRx_crf <= value; 
 endmethod

 method Action _inputs_TxReadyIn_From_Analyze (Bool value);
	wr_TxReady_In <= value; 
 endmethod 

 method Maybe#(FType13_ResponseClass) outputs_RxFtype13ResponseClass_ ();
 	return rg_Ftype13_ResponseClass;
 endmethod
 method Maybe#(Data) outputs_RxFtype13ResponseData_ ();
	return rg_RespClassData;
 endmethod 
 method ReceivedPktsInfo outputs_ReceivedPkts_ ();
	return ReceivedPktsInfo {headerpkt: rg_HeaderPkt, datapkt: rg_DataPkt, pktcount: rg_PktCount, lastpkt: rg_LastPkt };
 endmethod
 
 method TT outputs_TTReceived_ ();
	return rg_TTReceived; 
 endmethod
 method DestId outputs_RxDestId_ ();
	return rg_DestIDReceived;
 endmethod
 method SourceId outputs_RxSourceId_ ();
	return rg_SrcIDReceived;
 endmethod
 method Prio outputs_RxPrioField_ ();
	return rg_PrioReceived;
 endmethod
 method Bit#(4) outputs_MaxPktCount_ ();
	return pkt_Separation.outputs_MaxPktCount_ ();
 endmethod

endmodule : mkRapidIO_PktTransportParse

endpackage : RapidIO_PktTransportParse
