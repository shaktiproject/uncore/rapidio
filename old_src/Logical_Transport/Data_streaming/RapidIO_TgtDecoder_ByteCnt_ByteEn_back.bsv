/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO Read/Write Size and Word Pointer Decoder Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. To determine the values of Byte Count and Byte Enable using Read/Write Size and Word Pointer  
--    in the packet. 
-- 2. Designed as Combinational function. It doen't consume clock cycles, returns data 
--    in the same clock. 
-- 3. It can also be changed to sequential mode, by changing the output wires as registers.
-- 4. Used in Incoming Packet Receiver Side. 
-- 
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------
*/

/*
-- Inputs 
-- 1. Read - determines whether Read or Write
-- 2. Size - Read/Write
-- 3. Word Pointer
-- Outputs
-- 1. ByteCount - Contains number of Bytes to be transferred 
-- 2. ByteEn - Validates the bits present in the last packet
*/

package RapidIO_TgtDecoder_ByteCnt_ByteEn;

import RapidIO_DTypes ::*;

interface Ifc_RapidIO_TgtDecoder_ByteCnt_ByteEn;
// Input Ports as Methods
 method Action _inputs_Read (Bool value);
 method Action _inputs_Size (Size value);
 method Action _inputs_WdPtr (WdPointer value);

// Output Ports as Methods
 method ByteCount outputs_ByteCount_ ();
 method ByteEn outputs_ByteEn_ ();

endinterface : Ifc_RapidIO_TgtDecoder_ByteCnt_ByteEn

(* synthesize *)
//(* always_enabled *)
//(* always_ready *)
module mkRapidIO_TgtDecoder_ByteCnt_ByteEn (Ifc_RapidIO_TgtDecoder_ByteCnt_ByteEn);

// Input Methods as Wires
Wire#(Bool) wr_Read <- mkDWire (False);
Wire#(Size) wr_Size <- mkDWire (0);
Wire#(WdPointer) wr_WdPtr <- mkDWire (0);

// Output Methods as Registers
Reg#(ByteCount) reg_ByteCount <- mkReg (0);
Reg#(ByteEn) reg_ByteEn <- mkReg (0);

// Internal Wires
Wire#(ByteCount) wr_ByteCount <- mkDWire (0);
Wire#(ByteEn) wr_ByteEn <- mkDWire (0);

// -- Rules
/*
-- This rule determines the Byte Count and Byte Enable values. 
-- Case statement is used. 
*/
rule rl_Generate_ByteCount_ByteEn;

 case (wr_Size) matches

   4'b0000 :	begin
	wr_ByteCount <= 'd1;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00001000;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b10000000;		
	end

   4'b0001 :	begin
	wr_ByteCount <= 'd1;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00000100;	
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b01000000;
	end

   4'b0010 :	begin
	wr_ByteCount <= 'd1;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00000010;	
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b00100000;
	end

   4'b0011 :	begin
	wr_ByteCount <= 'd1;	
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00000001;	
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b00010000;
	end

   4'b0100 :	begin
	wr_ByteCount <= 'd2;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00001100;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11000000;
	end

   4'b0101 :	begin
	wr_ByteCount <= 'd3;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00000111;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11100000;	
	end

   4'b0110 :	begin
	wr_ByteCount <= 'd2;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00000011;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b00110000;
	end

   4'b0111 :	begin
	wr_ByteCount <= 'd5;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00011111;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11111000;
	end

   4'b1000 :	begin
	wr_ByteCount <= 'd4;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00001111;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11110000;
	end

   4'b1001 :	begin	
	wr_ByteCount <= 'd6;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b00111111;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11111100;
	end
	
   4'b1010 :	begin	
	wr_ByteCount <= 'd7;
	if (wr_WdPtr == 1)
		wr_ByteEn <= 8'b01111111;
	else if (wr_WdPtr == 0)
		wr_ByteEn <= 8'b11111110;
	end

   4'b1011 :	begin
	if (wr_WdPtr == 1)
		wr_ByteCount <= 'd16;
	else if (wr_WdPtr == 0) begin
		   wr_ByteCount <= 'd8;
		   wr_ByteEn <= 8'b11111111; 
		end
	end

   4'b1100 :	begin	
	if (wr_WdPtr == 1)
		wr_ByteCount <= 'd64;
	else if (wr_WdPtr == 0)
		wr_ByteCount <= 'd32;
	end

   4'b1101 :	begin
	if (wr_WdPtr == 1)
		wr_ByteCount <= 'd128; 
	else if ((wr_WdPtr == 0) && (wr_Read == True))
		wr_ByteCount <= 'd96;
	else if ((wr_WdPtr == 0) && (wr_Read == False))
		$display ("Oops!!!, The ByteCount value cannot be generated for Write.\n");
	end

   4'b1110 :	begin	
	if (wr_Read == True) begin
	   if (wr_WdPtr == 1) 
		wr_ByteCount <= 'd192; 
	   else if (wr_WdPtr == 0) 
		wr_ByteCount <= 'd160;
		end
	if (wr_Read == False)
		$display ("Oops!!!, The ByteCount value cannot be generated for Write.\n");
	end

   4'b1111 :	begin
	if (wr_WdPtr == 1)
		wr_ByteCount <= 'd256; 
	else if ((wr_WdPtr == 0) && (wr_Read == True))
		wr_ByteCount <= 'd224;
	else if ((wr_WdPtr == 0) && (wr_Read == False))
		$display ("Oops!!!, The ByteCount value cannot be generated for Write.\n");
	end

 endcase

endrule

rule rl_Saving_value_Reg;
	reg_ByteCount <= wr_ByteCount;
	reg_ByteEn <= wr_ByteEn;
endrule


// Input Ports as Methods
 method Action _inputs_Read (Bool value);
	wr_Read <= value;
 endmethod
 method Action _inputs_Size (Size value);
	wr_Size <= value;
 endmethod
 method Action _inputs_WdPtr (WdPointer value);
	wr_WdPtr <= value; 
 endmethod

// Output Ports as Methods
 method ByteCount outputs_ByteCount_ ();
	return wr_ByteCount;
 endmethod
 method ByteEn outputs_ByteEn_ ();
	return wr_ByteEn;
 endmethod

endmodule : mkRapidIO_TgtDecoder_ByteCnt_ByteEn

endpackage : RapidIO_TgtDecoder_ByteCnt_ByteEn
